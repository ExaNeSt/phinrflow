ifeq ($(OS),Windows_NT)
    ifeq ($(OSTYPE),cygwin)
    	include Makefile.unix
    else
  	include Makefile.windows
    endif
else
    include Makefile.unix
endif
