#include "applications.h"
#include "literal.h"
#include "globals.h"
#include "../kernels/trace.h"
#include "../kernels/patterns.h"
#include "../kernels/neighbours.h"
#include "../kernels/pseudoapps.h"
#include "../kernels/exanest.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <float.h>

/* *
 * * Print a send line in the standard output.
 * *
 * *@param from    The source node.
 * *@param to      The destination node.
 * *@param tag     The MPI tag for this message.
 * *@param size    The size in bytes of the message.
 * */
void insert_computation(application *app, list_t **task_events, long from, long computation)
{
    event ev;
    ev.ev_length = malloc(sizeof(struct event_length_t));
    
    ev.type = COMPUTATION;
    ev.id = 0;
    ev.ev_length->count = computation;
    ev.ev_length->length = computation;
//    ev.ev_length->allocated = 0;
    ev.pid = from;
    ev.pid2 = -1;
    ev.total_subflows = 0;
    ev.subflows_aux = 0;
    ev.app = app;
    //printf("C: %ld \n", computation);
    list_append(task_events[from], &ev);
}

/* *
 * * Print a send line in the standard output.
 * *
 * *@param from    The source node.
 * *@param to      The destination node.
 * *@param tag     The MPI tag for this message.
 * *@param size    The size in bytes of the message.
 * */
void insert_send(application *app, list_t **task_events, long from, long to, long tag, long size, int type)
{
    event ev;
    ev.ev_length = malloc(sizeof(struct event_length_t));
    
    ev.type = SENDING;
    ev.id = tag;
    ev.pid = from;
    ev.ev_length->length = size;
    ev.ev_length->count = size;
    ev.ev_length->latency_aux = 0;
  //  ev.ev_length->allocated = 0;
    ev.pid2 = to;
    ev.app = app;
    ev.total_subflows = 0;
    ev.subflows_aux = 0;
    //printf("S: %ld -> %ld \n", to, from);
    list_append(task_events[from], &ev);
}

void init_send_event(event *ev, long start_time){

    ev->dflow = malloc(sizeof(struct dflow_t));
    ev->dflow->speed = FLT_MAX;
    ev->dflow->path = malloc(sizeof(list_t));

}

void insert_recv(application *app, list_t **task_events, long to, long from, long tag, long size, int type)
{
    event ev;
    
    ev.type = RECEPTION;
    ev.id = tag;
    ev.pid = from;
    //ev.length = size;
    //ev.count = size;
    ev.pid2 = to;
    ev.total_subflows = 0;
    ev.subflows_aux = 0;
    ev.app = app;
    //printf("R: %ld -> %ld \n", to, from);
    list_append(task_events[from], &ev);
}

void send(application *app, list_t **task_events, long from, long to, long tag, long size)
{
    insert_send(app, task_events, from, to, tag, size, 0);
}

void receive(application *app, list_t **task_events, long to, long from, long tag, long size)
{
     insert_recv(app, task_events, to, from, tag, size, 0);
}

void gen_trace(application *app){

    switch(app->pattern){
        case ALL2ALL:
            all2all(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case MANYALL2ALL:
            many_all2all(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case MANYALL2ALLRND:
            many_all2all_rnd(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case ONE2ALL:
            one2all(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case ONE2ALLRND:
            one2all_rnd(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case ALL2ONE:
            all2one(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case ALL2ONERND:
            all2one_rnd(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
    	case PTP:
            ptp(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case RANDOM:
            randomapp(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case FILE_PATTERN:
            read_trc(app, app->task_events, app->size_tasks);
            break;
        case MESH2DWOC:
            mesh2d_woc(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case MESH2DWC:
            mesh2d_wc(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case MESH3DWOC:
            mesh3d_woc(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case MESH3DWC:
            mesh3d_wc(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case TORUS2DWOC:
            torus2d_woc(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case TORUS2DWC:
            torus2d_wc(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case TORUS3DWOC:
            torus3d_woc(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case TORUS3DWC:
            torus3d_wc(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case WATERFALL:
            waterfall(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case BINARYTREE:
            binarytree(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case BUTTERFLY:
            butterfly(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case GUPS:
            gups(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case NBODIES:
            nbodies(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case SHIFT:
            shift(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
         case BISECTION:
            bisection(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
         case HOTREGION:
            hotregion(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case MAPREDUCE:
            mapreduce(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case RANDOMAPP:
            randomapp(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case RANDOMAPPDCN:
            randomappdcn(app, app->task_events, app->size_tasks, app->packets, app->comp_time, app->phases);
            break;
        case DPSNN:
            dpsnn(app, app->task_events, app->size_tasks);
            break;
	case LAMMPS:
            lammps2(app, app->task_events, app->size_tasks);
            break;
        case GADGET:
            gadget(app, app->task_events, app->size_tasks);
            break;
        case REGCM:
            regcm2(app, app->task_events, app->size_tasks);
            break;
       default:
            printf("Undefined application.\n");
	    exit(-1);
            break;
    }
}
