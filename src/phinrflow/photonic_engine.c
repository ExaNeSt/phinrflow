#include "applications.h"
#include "mapping.h"
#include "workloads.h"
#include "dynamic_engine.h"
#include "photonic_engine.h"
#include "gen_trace.h"
#include "list.h"
#include "metrics.h"
#include "globals.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "float.h"
#include <math.h>
#include <assert.h>

int n_channels;
int n_lambdas;
long channel_bandwidth;
long n_inj;
channel_assignment_policy_t channel_assign_pol;
lambda_assignment_policy_t lambda_assign_pol;
list_t *path;

/**
 * Calculates the time in which the first event (CPU, flow sent) will finish or occur.
 */
//long time_next_event(list_t *list_cpus, list_t *list_flows){
double time_next_event_photonic(list_t *list_cpus, list_t *list_flows){

    long i;
    long found;
    //long min = LONG_MAX;
    double min = DBL_MAX;
    //long time_next = 0;
    double time_next = 0.0;
    long path_length = 0;
    application *app = NULL;
    event *ev = NULL;
    event **ev_p = NULL;
    long src = -1;
    long dst = -1;

    path = malloc(sizeof(list_t));
    list_reset(&running_applications);
    while(list_next(&running_applications, (void*)&app)){
        for(i = 0; i < app->size; i++){
	    app->comm_map = calloc(app->size, sizeof(int));
            list_reset(app->task_events_occurred[i]);
            while(list_next(app->task_events_occurred[i],(void*)&ev)){
                switch(ev->type){
                    case COMPUTATION:
                        if(ev->ev_length->count == ev->ev_length->length){
                            list_append(list_cpus, &ev);
                        }
                        break;
                    case SENDING:
                        if(!app->comm_map[ev->pid2] && (ev->ev_length->count == ev->ev_length->length)){
                            src = do_translation(app, ev->pid); 
                            dst = do_translation(app, ev->pid2); 
                            found = explore_routes(path,src, dst);
                            if(found == 0){
				app->comm_map[ev->pid2] = 1;
                                continue;
                            }
			    update_injection_flows_latency(&(app->info), (sched_info->makespan - ev->ev_length->latency_aux));

			    init_send_event(ev, sched_info->makespan);
                            ev->ev_length->latency_aux = sched_info->makespan;
                            path_length = mark_route_dynamic_photonic(app, ev->dflow, src, dst, path);
                            update_flows_distance(&(app->info), path_length);
                            list_append(list_flows, &ev);
                        }
                        break;
                    default:
                        break;
                }
            }
	    free(app->comm_map);
        }
    }
    list_reset(list_cpus);
    while(list_next(list_cpus, (void*)&ev_p)){
        if((*ev_p)->ev_length->count < min){
            min = (*ev_p)->ev_length->count;
        }
    }

    //min_links_bandwidth();

    list_reset(list_flows);
    while(list_next(list_flows, (void*)&ev_p)){
        time_next = round_ns((*ev_p)->ev_length->count / (*ev_p)->dflow->speed);
        if(time_next < min){
            min = time_next;
        }
    }
    free(path);

    return(min);
}

/**
 * Insert new events from a given task.
 */
long insert_new_events_photonic(application *app, long ntask){

    long done = 0;
    long num = 0;
    long src, dst, path, n_paths, length_subflow;

    event *ev;
    list_reset(app->task_events[ntask]);
    while(!done && list_next(app->task_events[ntask], (void*)&ev)){
        switch(ev->type){
            case COMPUTATION:
                done = 1;
                if(num == 0 && list_length(app->task_events_occurred[ntask]) == 0){
                    app->remaining_cpus[ev->pid]++;
                    list_append(app->task_events_occurred[ntask], ev);
                    list_rem_elem(app->task_events[ntask]);
                    num++;
                }
                else if( list_length(app->task_events[ntask]) > 0){
                    num = 1;
                }
                break;
            case SENDING:
                if(flow_inj_mode > app->remaining_flows[ev->pid] || flow_inj_mode == 0){
                    update_flows_number(&(app->info));
                    app->remaining_flows[ev->pid]++;
                    injected_flows++;
                    if(flow_inj_mode == app->remaining_flows[ev->pid])
                        done = 1;
                    if(load_balancing){
                        src = do_translation(app, ev->pid);
                        dst = do_translation(app, ev->pid2);
                        n_paths = get_n_paths_routing(src, dst);
                        length_subflow = (long)ceilf(ev->ev_length->length / n_paths);
                        ev->ev_length->length =  length_subflow;
                        ev->ev_length->count = length_subflow;
                    }
                    else{
                        n_paths = 1;
                    }
                    path = 0;
                    ev->total_subflows = n_paths;
                    ev->ev_length->latency_aux = sched_info->makespan;
                    update_subflows_number(&(app->info), n_paths);
                    while(path < n_paths){
                        ev->subflows_aux = path;
                        list_append(app->task_events_occurred[ntask], ev);
                        path++;
                    }
                    list_rem_elem(app->task_events[ntask]);
                }
                num++;
                break;
            case RECEPTION:
                num++;
                done = 1;
                break;
            default:
                printf("Unknown event type\n.");
                exit(-1);
        }
    }
    return(num);
}

long mark_route_dynamic_photonic(application *app, dflow_t *flow, long src, long dst, list_t *path)
{
    photonic_path_t *path_hop;
    int path_length = 0;


    list_reset(path);
    while(list_next(path, (void*)&path_hop)){
path_length++;	
        network[path_hop->node_id].port[path_hop->next_port].flows++;
        network[path_hop->node_id].opt_port[path_hop->next_port].channels[path_hop->channel_id].flows++;
        flow->speed = network[path_hop->node_id].opt_port[path_hop->next_port].channel_bandwidth;
        if(app->info.links_utilization[path_hop->node_id] == NULL){
            app->info.links_utilization[path_hop->node_id] = calloc(get_ports(path_hop->node_id), sizeof(int));
        }
        else if(app->info.links_utilization[path_hop->node_id][path_hop->next_port] == 0){
            app->info.num_links_used++;
        }
        app->info.links_utilization[path_hop->node_id][path_hop->next_port]++;
    }
    list_initialize(flow->path, sizeof(photonic_path_t));
    list_concat(flow->path,path);
    return(path_length);
}

long explore_routes(list_t* path, long src, long dst){

    long n_paths;
    long found = 0;
    long current_path = 0;

    list_initialize(path, sizeof(photonic_path_t));
    n_paths = get_n_paths_routing(src, dst);

    while (!found &&  current_path < n_paths){
        found = explore_route(path, src, dst);
        current_path++;
    }
    return(found);
}


long explore_route_static_static(list_t* path, long src, long dst){

    long current;
    short next_port, current_channel;
    long found = 0;
    photonic_path_t path_hop;

    current_channel = 0;
    current = src;
    init_routing(current, dst);
    next_port = route(current, dst);

    while(!found && current_channel < network[current].opt_port[next_port].n_channels){
        while(current != dst){	// not at destination yet
            if(network[current].opt_port[next_port].channels[current_channel].flows != 0){
                list_destroy(path);
                break;
            }
            set_path(&path_hop, current, next_port, current_channel, 0, NULL); 
            list_append(path, &path_hop);
            current = network[current].port[next_port].neighbour.node;
            next_port = route(current, dst);
            if(current == dst){
                found = 1;
            }
        }
        current_channel++;
        init_routing(src, dst);
        next_port = route(src, dst);
        current = src;
    }
    return(found);
}

long explore_route_adaptive_static(list_t* path, long src, long dst){

    int current_channel;
    long current;
    long next_port;
    long found = 1;
    photonic_path_t path_hop;

    current_channel = 0;
    init_routing(src, dst);
    next_port = route(src, dst);
    current = src;

    while(found && current != dst){	// not at destination yet

        while(current_channel < network[current].opt_port[next_port].n_channels){

            if(network[current].opt_port[next_port].channels[current_channel].flows == 0){
                set_path(&path_hop, current, next_port, current_channel, 0, NULL); 
                list_append(path, &path_hop);
                break;
            }
            current_channel++;
        }
        if(current_channel == network[current].opt_port[next_port].n_channels){
            found = 0;
            list_destroy(path);
            break;
        }
        current_channel = 0;
        current = network[current].port[next_port].neighbour.node;
        next_port = route(current, dst);
    }
    return(found);
}

void remove_flow_photonic(dflow_t *flow, long id, long src, long dst){

    photonic_path_t *path_hop;
    //list_t *dflows;
    //long node, port;
    //long path_n_apps = 0;

    list_reset(flow->path);
    while(list_next(flow->path, (void*)&path_hop)){
        network[path_hop->node_id].port[path_hop->next_port].flows--;
        network[path_hop->node_id].opt_port[path_hop->next_port].channels[path_hop->channel_id].flows--;
        //network[path_hop->node_id].port[path_hop->next_port].link_info_apps[id]--;
        //printf("RF: #%ld,%ld#\n",path->node_id, path->next_port);
        //if(network[path_hop->node_id].port[path_hop->next_port].link_info_apps[id] == 0){
       //     network[path_hop->node_id].port[path_hop->next_port].link_info_apps[0] = 0;
        //}
        //if(network[path_hop->node_id].port[path_hop->next_port].link_info_apps[0] > path_n_apps){
        //    path_n_apps = network[path_hop->node_id].port[path_hop->next_port].link_info_apps[0];
        //}


        //dflows = &network[path->node_id].port[path->next_port].dflows;
        //node = path->node;
        //port = path->next_port;
        //list_rem_node(&network[path->node_id].port[path->next_port].dflows, path->n_l);
        list_rem_elem(flow->path);
        //calc_remove_flow_max_min(dflows, node, port);
    }
    free(flow->path);
    free(flow);
}


void min_links_bandwidth_photonic(){
/*
    long i,j,i_aux, j_aux;
    float total_link_bw = 0.0;
    float link_bw = 0.0;
    float l_bandwidth = 0;
    float bandwidth = 0;
    list_t *dflows;
    dflow_t **flow;
    min_speed = FLT_MAX;
    avg_link_bandwidth = 0.0;
    links = 0;
    total_links = 0;
       
    for(i = 0; i < servers + switches; i++){
        for(j = 0; j < network[i].nports; j++){
            total_links++;
            if(network[i].port[j].neighbour.node == -1 || network[i].port[j].neighbour.port == -1 ||  network[i].port[j].flows == 0)
                continue;
            links++;
            link_bw = 0;
	    printf("a");
            //l_bandwidth = (float)network[i].port[j].bandwidth_capacity;
            //bandwidth = l_bandwidth / network[i].port[j].flows;
            //dflows = &network[i].port[j].dflows;
            //list_reset(dflows);
            //while(list_next(dflows, (void*)&flow)){
            //    if(bandwidth < (*flow)->speed && (*flow)->type != 5 && (*flow)->type != 6){
            //        (*flow)->speed = bandwidth;
            //    }
            //    if((*flow)->type != 5 && (*flow)->type != 6){
            //        link_bw += (*flow)->speed; 
            //    }
            //}
            // if(link_bw < min_speed)
            //     min_speed = link_bw;

            //total_link_bw += (link_bw / (float)l_bandwidth);
            // avg_link_bandwidth += link_bw;
        }
    }
    //avg_link_bandwidth /= links;
    //agg_bw += (((total_link_bw / links) - agg_bw) / (float)(++steps));
    //metrics.execution.avg_agg_bw += ((link_bw - metrics.execution.avg_agg_bw) / (float)(++metrics.execution.n_steps));
       if((dmetrics_time > 0 && (sched_info->makespan % dmetrics_time) == 0)){
       list_append(&metrics.execution.agg_bw, &agg_bw);
       agg_bw = 0.0;
       steps = 0;
       }
*/

}

void set_path(photonic_path_t *path_hop, long node_id, short next_port, short channel_id, short n_lambdas, short *lambdas){

    path_hop->node_id = node_id;
    path_hop->next_port = next_port;
    path_hop->channel_id = channel_id;
    path_hop->n_lambdas= n_lambdas;
    path_hop->lambdas = lambdas;;
}



