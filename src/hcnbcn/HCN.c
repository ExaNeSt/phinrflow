/** @mainpage
HCN(n,h) topology
**/

#include <stdlib.h>
#include <stdio.h>

#include "../phinrflow/node.h"

long param_a;	/* parameter a (= alpha), the number of masters adjacent to a switch in HCN(n,h) */
long param_b:	/* parameter b (= beta), the number of slaves adjacent to a switch in HCN(n,h) */
long param_h;	/* parameter h, depth of recursion of HCN(n,h) */

long *a_pow;	/* an array with each a^i, useful for doing some calculations, where 0 <= i <= h */
long masterservers;	/* the total number of masterservers: a^(h+1) */
long slaveservers;	/* the total number of slaveservers: a^h.b */
long servers; 	/* the total number of servers (= n): masterservers + slaveservers */
long switches;	/* the total number of switches: a^h */
long server_ports;	/* the number of ports on a server: 2
				   note: all switch nodes are assumed to have 2 ports even though
				   only one is used in HCN(n,h) (this is a prelude to coding BCN)
				   note: for every server, port 1 leads to a switch 
				   and port 2 leads to a server (or is empty for slaves) */
long switch_ports;	/* the number of ports on a switch: a + b*/

long *srcb; // the dimensional coordinates for the current node
long *dstb; // the dimensional coordinates for the destination

static char* network_token="HCN";
static char* routing_token="dimensional";			// NOT DONE
static char* topo_version="v0.1";				// NOT DONE
static char* topo_param_tokens[3]= {"a","b","h"};
//AE: make error checking so that we don't overrun this buffer
extern char filename_params[100];

/**
* Initializes the topology and sets the parameters a, b and h.
*/
long init_topo_HCN(long np, long* par) //THIS IS DONE I THINK
{
	long i;

	if (np<3) {
		printf("3 parameters are needed for HCN <a, b, h>\n");
		exit(-1);
	}
	param_a=par[0];
	param_b=par[1];
	param_h=par[2];
	sprintf(filename_params,"a%ldb%ldh%ld",param_a,param_b,param_h);

	param_n=param_a+param_b // set number of switch ports n = alpha + beta

	a_pow=malloc((param_h+1)*sizeof(long));
	a_pow[0]=1;

	for (i=1; i<=param_h; i++) {
		a_pow[i]=a_pow[i-1]*param_a;
	} // powers of a will be useful throughout,so let's compute them just once.

	masterservers=a_pow[param_h+1];
	slaveservers=a_pow[param_h]*param_b;
	servers=masterservers+slaveservers;
	switches=a_pow[param_h];
	ports=slaveservers+2*masterservers+switches*param_n;

	srcb=malloc((param_h+1)*sizeof(long));
	dstb=malloc((param_h+1)*sizeof(long));

	return 1; // return status, not used here
}

/**
* Release the resources used by the topology.
**/
void finish_topo_HCN()	// THIS IS DONE I THINK
{
	free(a_pow);
	free(srcb);
	free(dstb);
}


/**
* Get the number of servers of the network
*/
long get_servers_HCN()	// THIS IS DONE I THINK
{
	return servers;
}

/**
* Get the number of switches of the network
*/
long get_switches_HCN()	// THIS IS DONE I THINK
{
	return switches;
}

// Get the number of ports of a given node (either a server or a switch, see above)
long get_radix_HCN(long n)	// THIS IS DONE I THINK
{

	if (n<masterservers)
		return 2;	// if this is a master it has 2 ports
	else
		if (n<servers)	// if this is a slave it has 1 port
			return 1;
		else
			return param_n; // if this is a switch it has n ports
}

/**
* Calculates connections
**/

tuple_t connection_HCN(long node, long port)
{
	tuple_t res;
	long i;
	long sw=0;
	long lvl;
	long pos;

	\* assumption: 0 <= node < a^{h+1}+b(a^h)+a^h = masterservers+slaveservers+switches *\

	\* node is translated into dimensional form:
	   - if 0 <= node < masterservers: node => [i_h,i_{h-1},...,i_1,x] where 1 <= x <= a (master)
	     and each i_j in {0,1,...,a-1} (note that masterservers = a^{h+1})
	   - if masterservers <= node < servers:
	     node => [i_h,i_{h-1},...,i_1,y] where a+1 <= y <= a+b=n (slave) and 
	     each i_j in {0,1,...,a-1} (note that servers - masterservers = slaveservers = b(a^h))
	   - if servers <= node: node => [i_h,i_{h-1},...,i_1,0] (switch) where 
	     each i_j in {0,1,...,a-1} *\

	if (node < masterservers) {
		for(i=param_h; i>=0; i--) {
			srcb[i] = node/a_pow[i];
			node = node - srcb[i]*a_pow[i];
		}
		srcb[0] = srcb[0] + 1;	\* note that srcb[0] in {1,2,...,a} *\
	} else if (node < servers) {
			node = node - masterservers;	\* so now 0 <= node < slaveservers *\
			srcb[0] = node - (node/param_b)*param_b + param_a + 1;	
									\* note that srcb[0] in {a+1,a+2,...,a+b} *\
			node = (node/param_b)*param_a;
			for(i=param_h; i>=1; i--) {
				srcb[i] = node/a_pow[i];
				node = node - srcb[i]*a_pow[i];
			}
	} else {
		node = node - servers;	\* so no 0 <= node < switches *\
		srcb[0] = 0;
		node = node*param_a;
		for(i=param_h; i>=1; i--) {
			srcb[i] = node/a_pow[i];
			node = node - srcb[i]*a_pow[i];
		}
	}
			
	\* find dimensional form of res.node attached to node via port:
	   - if srcb[0] = 0: => node is a switch and assumed that 0 <= port < a+b=n
	     with res.node = [i_h,i_{h-1},...,i_1,port+1] and res.port = 0
	   - if 1 <= srcb[0] <= a: => node is a master and assumed that port in {0,1}
	     with res.node = [i_h,i_{h-1},...,i_1,0] if port = 0 and res.port = srcb[0]-1; and
	     with res.node = flip-node if (port = 1 and i_j <> i_{j-1} for some j in {h,h-1,...,1})
	     and res.port = 1
		(if (i_j = i_{j-1} for all j and port = 1) then there is an error)
	   - if a+1 <= srcb[0] <= a+b: => node is a slave and assumed that port = 0 
		(remember: 1 is officially allowed but only in BCN, not in HCN)
		with res.node = [i_h,i_{h-1},...,i_1,0] and res.port = srcb[0]-1 *\

	if (srcb[0] = 0) {
		dstb[0] = port+1;
		for(i=1; i<=param_h; i++) {
			dstb[i] = srcb[i];
		}
		res.port = 0;	
	} else if (1 <= srcb[0] and srcb[0] <= a) {
			if port = 0 {
				dstb[0] = 0;
				for(i=1; i<=param_h; i++) {
					dstb[i] = srcb[i];
				}
				res.port = srcb[0]-1;	
			} else {
				flipbit = 0;
				finished = 0;
				while (finished = 0) {
					if srcb[flipbit+1] = srcb[flipbit {
						flipbit = flipbit+1;
					} else {
						finished = 1;
					}
					if flipbit = param_h {
						finished = 1;
					}
				}
				if flipbit < param_h {
					for(i=param_h; i>flipbit+1; i--) {
						dstb[i] = srcb[i];
					}
					dstb[flipbit+1] = srcb[flipbit];
					for (i=flipbit; i>=0; i--) {
						dstb[i] = srcb[flipbit+1];
					}
				} else {
					\* there is an error *\
				}
			res.port = 1;
			} 
	} else {
		dstb[0] = 0;
		for(i=1; i<=param_h; i++) {
			dstb[i] = srcb[i];
		}
		res.port = 0;	
	}

	\* now transform dstb into its numeric form *\

	res.node = 0;
	for (i=1; i<=param_h; i++) {
		res.node = res.node + a_pow[i-1]*param_a;
		}
	if (1 <= dstb[0]) and (dstb[0] <= a) {
		res.node = res.node*param_a + dstb[0] - 1;
	} else if (a+1 <= dstb[0]) and (dstb[0] <= a+b) {
		res.node = masterservers + res.node*param_b + dstb[0] - a - 1;
	} else {
		res.node = servers + res.node;
	}
	return res;
}

long is_server_bcube(long i)
{
	return (i<servers);
}


char * get_network_token_bcube()
{
	return network_token;
}

char * get_routing_token_bcube()
{
	return routing_token;
}

char * get_topo_version_bcube()
{
	return topo_version;
}


char * get_topo_param_tokens_bcube(long i)
{
	return topo_param_tokens[i];
}

char * get_filename_params_bcube()
{
	return filename_params;
}

long get_server_i_bcube(long i)
{
	return i;
}

long get_switch_i_bcube(long i)
{
	return servers+i;
}


long node_to_server_bcube(long i)
{
	return i;
}

long node_to_switch_bcube(long i)
{
	return i-servers;
}

long get_ports_bcube()
{
	return ports;
}

long init_routing_bcube(long s, long d)
{
	return 0;
}

void finish_route_bcube()
{
}
/**
* Simple oblivious DOR. Others can be implemented.
*/
long route_bcube(long current, long destination)
{
	long port=0;
	long lvl;

	ui









#ifdef DEBUG
	if(current==destination) {
		printf("should not be routing a packet that has arrived to its destination (curr: %d, dstb: %d)!\n", current, destination);
		return -1; // just in case, let's abort the routing
	}
#endif // DEBUG

	if(current<servers) { // this is a server{
		while( (current % param_n) == (destination % param_n) ) {
			port++;
			current = current/param_n;
			destination=destination/param_n;
		}
	} else { // this is switch
		lvl=(current-servers)/n_pow[param_k-1]; // level of the switch
		//pos=(current-servers)%n_pow[param_k-1]; // position of the switch in that level
		port=(destination/n_pow[lvl])%param_n;
	}
	return port;
}

