#ifndef _collectives
#define _collectives

void barrier(application *app, list_t **task_events, int *group, int size_group, long *tag);

void alltoall(application *app, list_t **task_events, int *group, int size_group, long *tag, long data);

void alltoall_v(application *app, list_t **task_events, int *group, int size_group, long *tag, int **data);

void computation(application *app, list_t **task_events, int *group, int size_group, long max_time, long min_time);

void broadcast(application *app, list_t **task_events, int *group, int size_group, long *tag, int root, long data);

void allreduce(application *app, list_t **task_events, int *group, int size_group, long *tag, long data);

void scan(application *app, list_t **task_events, int *group, int size_group, long *tag, long data);

void scatter(application *app, list_t **task_events, int *group, int size_group, long *tag, int root, long data);

void gather(application *app, list_t **task_events, int *group, int size_group, long *tag, int root, long data);

//void allgather(application *app, list_t **task_event, int *group, int size_group, long **tag, int root, long data);

void alltoone(application *app, list_t **task_events, int *group, int size_group, long *tag, int dst, long data);
#endif

