#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "../phinrflow/gen_trace.h"

void barrier(application *app, list_t **task_events, int *group, int size_group, long *tag){

	int i, j;
	long tag_aux;
	
	int data = 4 * 8; //word 4 bytes=32bits
	
	tag_aux = *tag;
	for(i = 0; i < size_group; i++){
		for(j = 0; j < size_group; j++){
			if( group[i] != group[j]){
				send(app, task_events, group[i], group[j], tag_aux++, data);
			}
		}
	}

	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		for(j = 0; j < size_group; j++){
			if( group[i] != group[j]){
				receive(app, task_events, group[i], group[j], tag_aux++, data);
			}
		}
	}
	*tag = tag_aux;
}

void alltoall(application *app, list_t **task_events, int *group, int size_group, long *tag, long data){

	int i, j;
	long tag_aux;
	
	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		for(j = 0; j < size_group; j++){
			if( group[i] != group[j]){
				send(app, task_events, group[i], group[j], tag_aux++, data);
			}
		}
	}

	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		for(j = 0; j < size_group; j++){
			if( group[i] != group[j]){
				receive(app, task_events, group[i], group[j], tag_aux++, data);
			}
		}
	}
	*tag = tag_aux;
}

void alltoall_v(application *app, list_t **task_events, int *group, int size_group, long *tag, int **data){

	int i, j;
	long tag_aux;
	
	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		for(j = 0; j < size_group; j++){
			if( group[i] != group[j] && data[i][j] !=0){
				send(app, task_events, group[i], group[j], tag_aux++, data[i][j]);
			}
		}
	}

	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		for(j = 0; j < size_group; j++){
			if( group[i] != group[j] && data[i][j] !=0){
				receive(app, task_events, group[i], group[j], tag_aux++, data[i][j]);
			}
		}
	}
	
	*tag = tag_aux;
}

void computation(application *app, list_t **task_events, int *group, int size_group, long max_time, long min_time){

	int i;
	long time;
	
	for(i = 0; i < size_group; i++){
		time = (random() % (max_time - min_time)) + min_time;
		insert_computation(app, task_events, group[i], time);
	}
}


void broadcast(application *app, list_t **task_events, int *group, int size_group, long *tag, int root, long data){

	int i;
	long tag_aux;
	
	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		if(group[root] != group[i]){
			send(app, task_events, group[root], group[i], tag_aux++, data);
		}
	}

	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		if(group[root] != group[i]){
			receive(app, task_events, group[root], group[i], tag_aux++, data);
		}
	}

	*tag = tag_aux;

}

void allreduce(application *app, list_t **task_events, int *group, int size_group, long *tag, long data){

	int i, j;
	long tag_aux;
	
	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		for(j = 0; j < size_group; j++){
			if( group[i] != group[j]){
				send(app, task_events, group[i], group[j], tag_aux++, data);
			}
		}
	}

	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		for(j = 0; j < size_group; j++){
			if( group[i] != group[j]){
				receive(app, task_events, group[i], group[j], tag_aux++, data);
			}
		}
	}
	*tag = tag_aux;

}


void scan(application *app, list_t **task_events, int *group, int size_group, long *tag, long data){

	int i, j;
	long tag_aux;
	
	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		for(j = 0; j < size_group; j++){
			if( group[i] < group[j]){
				send(app, task_events, group[i], group[j], tag_aux++, data);
			}
		}
	}

	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		for(j = 0; j < size_group; j++){
			if( group[i] < group[j]){
				receive(app, task_events, group[i], group[j], tag_aux++, data);
			}
		}
	}
	*tag = tag_aux;


}

void scatter(application *app, list_t **task_events, int *group, int size_group, long *tag, int root, long data){

	int i;
	long tag_aux;
	
	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		if(group[root] != group[i]){
			send(app, task_events, group[root], group[i], tag_aux++, data);
		}
	}

	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		if(group[root] != group[i]){
			receive(app, task_events, group[root], group[i], tag_aux++, data);
		}
	}

	*tag = tag_aux;


}

void gather(application *app, list_t **task_events, int *group, int size_group, long *tag, int root, long data){

	int i;
	long tag_aux;
	
	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		if(group[root] != group[i]){
			send(app, task_events, group[i], group[root], tag_aux++, data);
		}
	}

	tag_aux = *tag;

	for(i = 0; i < size_group; i++){
		if(group[root] != group[i]){
			receive(app, task_events, group[i], group[root], tag_aux++, data);
		}
	}

	*tag = tag_aux;

}

//void allgather(application *app, list_t **task_event, int *group, int size_group, long **tag, int root, long data){


//}

void alltoone(application *app, list_t **task_events, int *group, int size_group, long *tag, int dst, long data){

    long i;
    long tag_aux;

    tag_aux = *tag;

        for (i = 0; i < size_group; i++){	
            if(dst != group[i]){
                send(app, task_events, group[i], dst, tag_aux, data);
                receive(app, task_events, group[i], dst, tag_aux, data);
                tag_aux++;
            }
        }
	*tag = tag_aux;
}


