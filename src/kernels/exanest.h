#ifndef _exanest
#define _exanest

void dpsnn(application *app, list_t **task_events, long nodes);

void prepare_data(int nprocs, int **data, int **data_proc, int n_grid_proc, int **grid_procs, int grid_x, int grid_y, int **grid, int *grid_data, int synapses_grid);

void prepare_init_data(int nprocs, int **data, int n_grid_proc, int **grid_procs, int grid_x, int grid_y, int **grid, int *grid_data, int synapses_grid);

void dpsnn_assign_grid_procs(int **grid_procs, int **grid, int nprocs, int n_grid_procs);

void dpsnn_select_destinations(int **grid, int type, int grid_x, int grid_y, int *grid_data);

void assign_node(int ** grid, int node_aux, int ind, int ind_x, int ind_y, int grid_x, int grid_y);

void lammps(application *app, list_t **task_events, long nodes);

void lammps_p2p_sends(int me, application *app,list_t **task_events, int *group, long *tag, int nodes, int data);

void gadget(application *app, list_t **task_events, long nodes);

void regcm(application *app, list_t **task_events, long nodes);

void lammps2(application *app, list_t **task_events, long nodes);

void lammps2_movement_east(application *app, list_t **task_events, int px, int py, int pz, int i, int j, int k, long tag, long data);

void regcm2(application *app, list_t **task_events, long nodes);

#endif
