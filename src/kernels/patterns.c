#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "../phinrflow/list.h"
#include "../phinrflow/gen_trace.h"

/**
 * Prints a trace in the standard output performing a all-to-all interchange. (non-optimized)
 *
 *@param nodes	The number of nodes. It should be a power of two value, if not an error is printed in the error output.
 *@param size	The size of the burst.
 */
void all2all(application *app, list_t **task_events, long nodes, long size, long computation, long phases)
{
    long i, j, k;
    long tag_s = 0;
    long tag_r = 0;

    // Check whether th number of phases is given or not. If not, set to 1.
    if(phases == -1)
        phases = 1;

    for (k = 0; k < phases; k++) {
        tag_s++;
        for (i = 0; i < nodes; i++){	
            //insert_computation(app, task_events, i, computation);
            for (j = 0; j < nodes; j++){
                if(i != j)
                    send(app, task_events, i, j, tag_s++, size);
            }
        }
        tag_r++;
        for (i = 0; i < nodes; i++){	
            for (j = 0; j < nodes; j++){
                if(i != j)
                    receive(app, task_events, i, j, tag_r++, size);
            }
        }
    }
}

void many_all2all(application *app, list_t **task_events, long nodes, long size, long computation, long phases){

    long i, j;
    long src = 0;
    long dst = 0;
    long curr_group = 0;
    long group_size = app->pattern_params[1]; 
    long tag_s = 0;
    long tag_r = 0;

    // Check whether the number of phases is given or not. If not, set to 1.
    if(phases == -1)
        phases = 1;


    for (j = 0; j < phases; j++){
        for(i = 0; i < nodes; i++){
            //insert_computation(app, task_events, 0, computation);
        }
        src = 0;
        dst = 0;
        curr_group = 0;
        while(src != -1 || dst != -1){
            send(app, task_events, src, dst, tag_s++, size);
            src++;
            if(src % group_size == 0 || src == nodes) {
                src = curr_group * group_size;
                dst++;
                if(dst == nodes) {
                    src = -1;
                    dst = -1;
                } 
                else if(dst % group_size == 0) {
                    curr_group++;
                    src = curr_group * group_size;
                }
            }
        }
        src = 0;
        dst = 0;
        curr_group = 0;
        while(src != -1 || dst != -1){
            receive(app, task_events, src, dst, tag_r++, size);
            src++;
            if(src % group_size == 0 || src == nodes) {
                src = curr_group * group_size;
                dst++;
                if(dst == nodes) {
                    src = -1;
                    dst = -1;
                } 
                else if(dst % group_size == 0) {
                    curr_group++;
                    src = curr_group * group_size;
                }
            }
        }

    }
}

void many_all2all_rnd(application *app, list_t **task_events, long nodes, long size, long computation, long phases){

    long i, j, r, tmp;
    long src = 0;
    long dst = 0;
    long curr_group = 0;
    long group_size = app->pattern_params[1]; 
    long *translate;
    long tag_s = 0;
    long tag_r = 0;

    // Check whether th number of phases is given or not. If not, set to 1.
    if(phases == -1)
        phases = 1;


    translate=malloc(nodes * sizeof(long));

    for (i = 0; i < nodes; i++) {
        translate[i] = i;
    }
    for (i = 0; i < nodes; i++) {
        r = rand() % nodes;
        tmp = translate[i];
        translate[i] = translate[r];
        translate[r] = tmp;
    }

    for (j = 0; j < phases; j++){
        for(i = 0; i < nodes; i++){
            //insert_computation(app, task_events, translate[i], computation);
        }
        src = 0;
        dst = 0;
        curr_group = 0;
        while(src != -1 || dst != -1){
            send(app, task_events, translate[src], translate[dst], tag_s++, size);
            src++;
            if(src % group_size == 0 || src == nodes) {
                src = curr_group * group_size;
                dst++;
                if(dst == nodes) {
                    src = -1;
                    dst = -1;
                } 
                else if(dst % group_size == 0) {
                    curr_group++;
                    src = curr_group * group_size;
                }
            }
        }
        src = 0;
        dst = 0;
        curr_group = 0;
        while(src != -1 || dst != -1){
            receive(app, task_events, translate[src], translate[dst], tag_r++, size);
            src++;
            if(src % group_size == 0 || src == nodes) {
                src = curr_group * group_size;
                dst++;
                if(dst == nodes) {
                    src = -1;
                    dst = -1;
                } 
                else if(dst % group_size == 0) {
                    curr_group++;
                    src = curr_group * group_size;
                }
            }
        }

    }
    free(translate);
}

/**
 * Prints a trace in the standard output performing a one-to-all interchange. (non-optimized bcast)
 *
 *@param nodes	The number of nodes.
 *@param size	The size of the burst.
 */
void one2all(application *app, list_t **task_events, long nodes, long size, long computation, long phases)
{
    long i, l;
    long tag_s = 0;
    long tag_r = 0;

    // Check whether th number of phases is given or not. If not, set to 1.
    if(phases == -1)
        phases = 1;

    for (l = 0; l < phases; l++){
        for (i = 1; i < nodes; i++)	
        {	
            send(app, task_events, 0, i, tag_s++, size);
            receive(app, task_events, 0, i, tag_r++, size);
        }
    }
}

/**
 * Prints a trace in the standard output performing a one-to-all interchange. (non-optimized bcast)
 *
 *@param nodes	The number of nodes.
 *@param size	The size of the burst.
 */
void one2all_rnd(application *app, list_t **task_events, long nodes, long size, long computation, long phases)
{
    long i, l;
    long src = rand() % nodes;
    long tag_s = 0;
    long tag_r = 0;

    // Check whether th number of phases is given or not. If not, set to 1.
    if(phases == -1)
        phases = 1;

    for (l = 0; l < phases; l++){
        for (i = 0; i < nodes; i++)	
        {	
            if(src != i){
                send(app, task_events, src, i, tag_s++, size);
                receive(app, task_events, src, i, tag_r++, size);
            }
        }
    }
}

/**
 * Prints a trace in the standard output performing a all-to-one interchange. (non-optimized reduce)
 *
 *@param nodes	The number of nodes.
 *@param size	The size of the burst.
 */
void all2one(application *app, list_t **task_events, long nodes, long size, long computation, long phases)
{
    long i, l;
    long tag_s = 0;
    long tag_r = 0;

    // Check whether th number of phases is given or not. If not, set to 1.
    if(phases == -1)
        phases = 1;


    for (l = 0; l < phases; l++){
        for (i = 1; i < nodes; i++)	
        {	
            send(app, task_events, i, 0, tag_s++, size);
            receive(app, task_events, i, 0, tag_r++, size);
        }
    }

}

/**
 * Prints a trace in the standard output performing a all-to-one interchange. (non-optimized reduce)
 *
 *@param nodes	The number of nodes.
 *@param size	The size of the burst.
 */
void all2one_rnd(application *app, list_t **task_events, long nodes, long size, long computation, long phases)
{
    long i, l;
    long dst = rand() % nodes;
    long tag_s = 0;
    long tag_r = 0;


    // Check whether th number of phases is given or not. If not, set to 1.
    if(phases == -1)
        phases = 1;

    for (l = 0; l < phases; l++){
        for (i = 0; i < nodes; i++)	
        {	
            if(dst != i){
                send(app, task_events, i, dst, tag_s++, size);
                receive(app, task_events, i, dst, tag_r++, size);
            }
        }
    }
}

/**
 * Prints a trace in the standard output performing a all-to-one interchange. (non-optimized reduce)
 *
 *@param nodes	The number of nodes.
 *@param size	The size of the burst.
 */
void ptp(application *app, list_t **task_events, long nodes, long size, long computation, long phases)
{
    long l;
    long tag_s = 0;
    long tag_r = 0;
    //long tag_c = 0;

    // Check whether th number of phases is given or not. If not, set to 1.
    if(phases == -1)
        phases = 1;

    for (l = 0; l < phases; l++){

        send(app, task_events, 0, 1, tag_s++, 10000);
        send(app, task_events, 1, 0, tag_s++, 10000);
        receive(app, task_events, 0, 1, tag_r++, 10000);
        receive(app, task_events, 1, 0, tag_r++, 10000);

//        write(app, task_events, 0, 64, &tag_c, 10000, 10);
//        read(app, task_events, 0, 64, &tag_c, 10, 10000);
        //send(app, task_events, 1, 2, tag_s++, 1000);
        //send(app, task_events, 1, 2, tag_s++, 10000);
        //send(app, task_events, 0, 9, tag_s++, 10000);
        //send(app, task_events, 1, 10, tag_s++, 100);
        //receive(app, task_events, 1, 2, tag_r++, 1000);
        //receive(app, task_events, 1, 2, tag_r++, 10000);
        //receive(app, task_events, 0, 9, tag_r++, 10000);
        //receive(app, task_events, 1, 10, tag_r++, 100);
    }
}

/**
 * Initializes the random pattern. It requires 1 parameter: the total number of flows. Flows are generated independently: source and destination are selected randomly using a uniform distribution.
 * @param nservers The number of communicating servers
 * @param nparam The number of parameters in the list of parameters
 * @param params A list with the parameters for the traffic pattern
 */
void randomapp(application *app, list_t **task_events, long nodes, long size, long computation, long phases)
{
    long i, j, l, tag_s, tag_r,seed, src, dst;
    char statenew[256];
    char *stateold;
    long groups = app->pattern_params[2]; 
    long group_size;
    long size_aux;

    if(groups <= 0){
        printf("Groups size must be greater than 0.");
        exit(-1);
    }
    group_size = app->pattern_params[1] / groups;

    // Check whether th number of phases is given or not. If not, set to 1.
    if(phases == -1)
        phases = 1;

    tag_s = 0;
    tag_r = 0;
    for (l = 0; l < phases; l++){
        for(j =0; j < groups; j++){
            seed = rand();
            stateold = initstate(seed, statenew, 256);
            for(i = 0; i < group_size;i++){
                src = random()%nodes;
                dst = random()%nodes;
                while(src == dst){
                    src = random()%nodes;
                    dst = random()%nodes;
                }
		size_aux = random()%40000000000;
                send(app, task_events, src, dst, tag_s++, size_aux);
            }
            setstate(stateold);
            stateold = initstate(seed, statenew, 256);
            for(i = 0; i < group_size;i++){
                src = random()%nodes;
                dst = random()%nodes;
                while(src == dst){
                    src = random()%nodes;
                    dst = random()%nodes;
                }
		size_aux = random()%40000000000;
                receive(app, task_events, src, dst, tag_r++, size_aux);
            }
            setstate(stateold);
        }
    }
}

/**
 * Initializes the random pattern. It requires 1 parameter: the total number of flows. Flows are generated independently: source and destination are selected randomly using a uniform distribution.
 * @param nservers The number of communicating servers
 * @param nparam The number of parameters in the list of parameters
 * @param params A list with the parameters for the traffic pattern
 */
void randomappdcn(application *app, list_t **task_events, long nodes, long size, long computation, long phases)
{
    long i, j, l, tag_s, tag_r,seed, src, dst;
    char statenew[256];
    char *stateold;
    long groups = app->pattern_params[2]; 
    long group_size;

    if(groups <= 0){
        printf("Groups size musb be greater than 0.");
        exit(-1);
    }
    group_size = app->pattern_params[1] / groups;

    // Check whether th number of phases is given or not. If not, set to 1.
    if(phases == -1)
        phases = 1;

    tag_s = 0;
    tag_r = 0;
    for (l = 0; l < phases; l++){
        for(j =0; j < groups; j++){
            seed = rand();
            stateold = initstate(seed, statenew, 256);
            for(i = 0; i < group_size;i++){
                src = random()%nodes;
                dst = random()%nodes;
                while(src == dst){
                    src = random()%nodes;
                    dst = random()%nodes;
                }
                if((random() % 100) < 80){
                    size = (random() % 16) + 1;
                }
                else {
                    size = 16 + (random() % 1486);
                }
                send(app, task_events, src, dst, tag_s++, size * 8);
            }
            setstate(stateold);
            stateold = initstate(seed, statenew, 256);
            for(i = 0; i < group_size;i++){
                src = random()%nodes;
                dst = random()%nodes;
                while(src == dst){
                    src = random()%nodes;
                    dst = random()%nodes;
                }
                if((random() % 100) < 80){
                    size = (random() % 16) + 1;
                }
                else {
                    size = 16 + (random() % 1486);
                }

                receive(app, task_events, src, dst, tag_r++, size * 8);
            }
            setstate(stateold);
        }
    }
}

