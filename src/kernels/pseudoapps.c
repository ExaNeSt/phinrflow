#include "../phinrflow/gen_trace.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Prints a trace in the standard output emulating gups (giga update per second). (random)
 * 
 *@param nodes	The number of nodes. It should be a power of two value, if not an error is printed in the error output.
 *@param size	The size of the burst.
 */
void gups(application *app, list_t **task_events, long nodes, long size, long computation, long phases)
{
	long i;   ///< The iteration.
	long s;  ///< The source node.
	long d; ///< The destination node.
	long tag_s = 0;
	long tag_r = 0;

	for (i=0; i<phases; i++)
	{
		do{
			s=rand()%nodes;
			d=rand()%nodes;
		} while(s==d);
		send(app, task_events, s, d, tag_s++, size);
		receive(app, task_events, d, s , tag_r++, size);
	}
}

/**
 * Prints a trace in the standard output emulating gups (giga update per second). (random)
 * 
 *@param nodes	The number of nodes. It should be a power of two value, if not an error is printed in the error output.
 *@param size	The size of the burst.
 */
void nbodies(application *app, list_t **task_events, long nodes, long size, long computation, long phases)
{
	long l, i ,s;
	long tag = 0;
	long size_aux = size / nodes;

	for (l = 0; l < phases; l++){
		for (s = 0; s < nodes; s++){
			send(app, task_events, s, (s + 1) % nodes, tag, size_aux);
			for (i = 0; i < (nodes / 2) - 1; i++){
				receive(app, task_events, (s + i) % nodes, (s + i + 1) % nodes, tag++, size_aux);
				send(app, task_events, (s + i + 1) % nodes, (s + i + 2) % nodes, tag, size_aux);
			} 
			receive(app, task_events, (s + i) % nodes, ( s+ i + 1) % nodes, tag++, size_aux);
			send(app, task_events, (s + i + 1) % nodes, s, tag, size_aux);
			receive(app, task_events, (s + i + 1) % nodes, s, tag++, size_aux);
		}
	}	
}

/**
 * The Shift pattern requires a single parameter, the stride. Each node will communicate with the node whose address is src+stride.
 */
void shift(application *app, list_t **task_events, long nodes, long size, long computation, long phases){

	long i, src, dst;
	long stride = app->pattern_params[1];
	long tag_s = 0;
	long tag_r = 0;

	for (i = 0; i < phases; i++){
		for(src = 0; src < nodes; src++){
			dst = (src + stride) % nodes;
			send(app, task_events, src, dst, tag_s++, size);
		}
		for(src = 0; src < nodes; src++){
			dst = (src + stride) % nodes;
			receive(app, task_events, src, dst, tag_r++, size);
		}
	}
}

/**
 * Initializes the bisection pattern which requires no parameter. The network is split into two randomly and each node will communicate with a node in the other half at random.
 * @param nservers The number of communicating servers
 * @param nparam The number of parameters in the list of parameters
 * @param params A list with the parameters for the traffic pattern
 */
void bisection(application *app, list_t **task_events, long nodes, long size, long computation, long phases){

	long i, r, tmp, max_flows;
	long *shuffle;
	long curr_flows;
	long tag_s = 0;
	long tag_r = 0;

	max_flows= 2 * (nodes / 2);//integer division;

	shuffle=malloc(nodes * sizeof(long));

	for (i = 0; i < nodes; i++){
		shuffle[i]=i;
	}
	for (i=0; i < nodes; i++){
		r = ztm(nodes - i);
		tmp = shuffle[i];
		shuffle[i] = shuffle[i + r];
		shuffle[i + r] = tmp;
	}

	for (i = 0; i < phases; i++){
		curr_flows = 0;
		while(curr_flows < max_flows){ 
			if(curr_flows % 2){
				send(app, task_events, shuffle[curr_flows], shuffle[curr_flows - 1], tag_s++, size);
				receive(app, task_events, shuffle[curr_flows], shuffle[curr_flows - 1], tag_r++, size);
			} else{
				send(app, task_events, shuffle[curr_flows], shuffle[curr_flows + 1], tag_s++, size);
				receive(app, task_events, shuffle[curr_flows], shuffle[curr_flows + 1], tag_r++, size);
			}
			curr_flows++;
		}
	}
	free(shuffle);
}

void hotregion(application *app, list_t **task_events, long nodes, long size, long computation, long phases){

	long i, j, k, src, dst, seed;
	char statenew[256];
	char *stateold;
	long groups = app->pattern_params[2]; 
	long tag_s = 0;
	long tag_r = 0;
	long group_size;

	if(groups <= 0){
		printf("Groups size musb be greater than 0.");
		exit(-1);
	}

	group_size = app->pattern_params[1] / groups;

	if(phases == -1)
		phases = 1;

	for (k = 0; k < phases; k++){
		for(j =0; j < groups; j++){
			seed = rand();
			stateold = initstate(seed, statenew, 256);
			for(i = 0; i < group_size;i++){
				src = random() % (nodes / 8);
				if(random() % 100 < 25){
					dst = random() % (nodes / 8);
					while(dst == src)
						dst = random() % (nodes / 8);
				}
				else{
					dst = random() % nodes;
					while(dst == src)
						dst = random() % nodes;
				}
				send(app, task_events, src, dst, tag_s++, size);
			}
			setstate(stateold);
			stateold = initstate(seed, statenew, 256);
			for(i = 0; i < group_size;i++){
				src = random() % (nodes / 8);
				if(random() % 100 < 25){
					dst = random() % (nodes / 8);
					while(dst == src)
						dst = random() % (nodes / 8);
				}
				else{
					dst = random() % nodes;
					while(dst == src)
						dst = random() % nodes;
				}
				receive(app, task_events, src, dst, tag_r++, size);
			}
			setstate(stateold);
		}
	}
}

void mapreduce(application *app, list_t **task_events, long nodes, long size, long computation, long phases){

	long i, j, k, seed;
	long tag_s = 0;
	long tag_r = 0;
	char statenew[256];
	char *stateold;
	long percent = app->pattern_params[1];
	// Check whether th number of phases is given or not. If not, set to 1.
	if(phases < 1)
		phases = 1;

	for (i = 1; i < nodes; i++)	{	
		send(app, task_events, 0, i, tag_s++, size);
		receive(app, task_events, 0, i, tag_r++, size);
	}

	for (k = 0; k < phases; k++){
		seed = rand();
		stateold = initstate(seed, statenew, 256);
		for (i = 1; i < nodes; i++){	
			//insert_computation(app, task_events, i, computation);
			for (j = 1; j < nodes; j++){
				if(i != j && ((random() % 100) < percent))
					send(app, task_events, i, j, tag_s++, size);
			}
		}
		setstate(stateold); 
		stateold = initstate(seed, statenew, 256);
		for (i = 1; i < nodes; i++){	
			for (j = 1; j < nodes; j++){
				if(i != j && ((random() % 100) < percent))
					receive(app, task_events, i, j, tag_r++, size);
			}
		}
		setstate(stateold);
	}
	for (i = 1; i < nodes; i++)	{	
		send(app, task_events, i, 0, tag_s++, size);
		receive(app, task_events, i, 0, tag_r++, size);
	}
}





