#include "../phinrflow/gen_trace.h"
#include "exanest.h"
#include "collectives.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * grid_x grid_y 
 */
void dpsnn(application *app, list_t **task_events, long nodes){

    int i, j, n_grid_proc;

    long tag = 0;
    int *communicator_0 = malloc(nodes * sizeof(int));
    int comp_base = 5000; // usecs
    int max_columns = 50;
    int **grid, **grid_procs, **data, **data_proc;
    int *grid_data;
    int grid_x, grid_y, neurons, synapses, selection_type, phases;

    grid_x = app->pattern_params[0]; 
    grid_y = app->pattern_params[1];
    neurons = app->pattern_params[2];
    synapses = app->pattern_params[3];
    selection_type = app->pattern_params[4];
    phases = app->pattern_params[5];

    grid = (int**)malloc(grid_x * grid_y * sizeof(int*));
    for( i = 0; i < grid_x * grid_y; i++){
        grid[i] = (int*)malloc(14 * sizeof(int));
        for(j = 0; j < 14; j++){
            grid[i][j] = -1;
        }
    }

    grid_data = malloc(13 * sizeof(int));

    n_grid_proc = (grid_x * grid_y) / app->size;
    grid_procs = (int**)malloc(app->size * sizeof(int*));
    for( i = 0; i < app->size; i++){
        grid_procs[i] = (int*)malloc(n_grid_proc * sizeof(int));
        for( j = 0; j < n_grid_proc; j++){
            grid_procs[i][j] = -1;
        }
    }

    data_proc = (int**)malloc(app->size * sizeof(int*));
    data = (int**)malloc(app->size * sizeof(int*));
    for( i = 0; i < app->size; i++){
        data_proc[i] = (int*)malloc(app->size * sizeof(int));
        data[i] = (int*)malloc(app->size * sizeof(int));
        for( j = 0; j < app->size; j++){
            data_proc[i][j] = -1;
            data[i][j] = 0;
        }
    }

    dpsnn_select_destinations(grid, selection_type, grid_x, grid_y, grid_data);

    dpsnn_assign_grid_procs(grid_procs, grid, app->size, n_grid_proc);




    for( i = 0; i < nodes; i++){
        communicator_0[i] = i;
    }

    // Phase 1
    // Step 1

    //computation(app, task_events, communicator_0, nodes, comp_base, comp_base * 1.1);

    //barrier(app, task_events, communicator_0, nodes, &tag);

    //computation(app, task_events, communicator_0, nodes, comp_base * 1000, comp_base * 2);

    // Step 2

    //alltoall(app, task_events, communicator_0, nodes, &tag, nodes * 4 * 8);

    //computation(app, task_events, communicator_0, nodes, comp_base * 1000, comp_base * 2);

   // prepare_init_data(app->size, data, n_grid_proc, grid_procs, grid_x, grid_y, grid, grid_data, neurons * synapses);

    //alltoall_v(app, task_events, communicator_0, nodes, &tag, data);

    //barrier(app, task_events, communicator_0, nodes, &tag);

    // Phase 2
    // Step 1


    for(i = 0; i < phases; i++){
        prepare_data(app->size, data, data_proc, n_grid_proc, grid_procs, grid_x, grid_y, grid, grid_data, neurons * synapses);

        alltoall_v(app, task_events, communicator_0, nodes, &tag, data_proc);

        barrier(app, task_events, communicator_0, nodes, &tag);

        alltoall_v(app, task_events, communicator_0, nodes, &tag, data);

        barrier(app, task_events, communicator_0, nodes, &tag);
    }



    free(communicator_0);

    free(grid_data);

    for(i = 0; i < app->size; i++){
        free(data_proc[i]); 
        free(data[i]); 
    }
    free(data_proc);
    free(data);

    for(i = 0; i < n_grid_proc; i++){
        free(grid_procs[i]); 
    }
    free(grid_procs);

    for(i = 0; i < grid_x * grid_y; i++){
        free(grid[i]); 
    }
    free(grid);

}


void prepare_data(int nprocs, int **data, int **data_proc, int n_grid_proc, int **grid_procs, int grid_x, int grid_y, int **grid, int *grid_data, int synapses_grid) {

    int i, j, k, n;
    int grid_elem_id, dest_proc, grid_elem_id_send, synapse_prob;

    for(i = 0; i < nprocs; i++){
        for(j = 0; j < nprocs; j++){
            data_proc[i][j] = -1;
            data[i][j] = 0;
        }
    }

    for( i = 0;i < nprocs;i++){

        for(j = 0; j < n_grid_proc;j++){

            grid_elem_id = grid_procs[i][j];
            n = 0;	
            while(n < (synapses_grid / 10)){
                synapse_prob = random() % 100;
                k = random() % 13;
                if(grid_data[k] < synapse_prob){
                    grid_elem_id_send = grid[grid_elem_id][k];
                    if(grid_elem_id_send != -1){
                        dest_proc = grid[grid_elem_id_send][13];
                        //printf("%d ", grid_data[k]);
                        data_proc[i][dest_proc] = (4 * 8);
                        data[i][dest_proc] += (4 * 8);
                        n++;
                    }
                }
            }

        }

    }


}


void prepare_init_data(int nprocs, int **data, int n_grid_proc, int **grid_procs, int grid_x, int grid_y, int **grid, int *grid_data, int synapses_grid) {

    int i, j, k;
    int grid_elem_id, dest_proc, grid_elem_id_send;

    for( i = 0;i < nprocs;i++){

        for(j = 0; j < n_grid_proc;j++){

            grid_elem_id = grid_procs[i][j];

            for(k = 0;k < 13;k++){

                grid_elem_id_send = grid[grid_elem_id][k];
                if(grid_elem_id_send != -1){
                    dest_proc = grid[grid_elem_id_send][13];
                    //printf("%d ", grid_data[k]);
                    data[i][dest_proc] += (int)(synapses_grid / grid_data[k]) * 4 * 8;
                }
            }

        }

    }


}

void dpsnn_assign_grid_procs(int **grid_procs, int **grid, int nprocs, int n_grid_procs){

    int i, j;
    int grid_elem = 0;

    for(i = 0; i < nprocs; i++){
        for(j = 0; j < n_grid_procs; j++){
            grid[grid_elem][13] = i;
            grid_procs[i][j] = grid_elem++;
        }
    }

}

void dpsnn_select_destinations(int **grid, int type, int grid_x, int grid_y, int *grid_data){

    int i, j, i_aux, j_aux, ind, node_aux;

    switch(type){

        case 0:
        case 1:
        case 2:
            grid_data[0] = 1; 
            grid_data[1] = 2; 
            grid_data[2] = 3; 
            grid_data[3] = 2; 
            grid_data[4] = 1; 
            grid_data[5] = 3; 
            grid_data[6] = 76; 
            grid_data[7] = 3; 
            grid_data[8] = 1; 
            grid_data[9] = 2; 
            grid_data[10] = 3; 
            grid_data[11] = 2; 
            grid_data[12] = 1; 

            for(i = 0; i < grid_x; i++){
                for(j = 0; j < grid_y; j++){
                    //printf("%d:  ", i);
                    node_aux = (i * grid_x) + j;
                    ind = 0;
                    i_aux = i - 2;
                    j_aux = j;
                    assign_node(grid, node_aux, ind, i_aux, j_aux, grid_x, grid_y);
                    ind++;
                    i_aux = i - 1;
                    j_aux = j - 1;
                    assign_node(grid, node_aux, ind, i_aux, j_aux, grid_x, grid_y);
                    ind++;
                    i_aux = i - 1;
                    j_aux = j;
                    assign_node(grid, node_aux, ind, i_aux, j_aux, grid_x, grid_y);
                    ind++;
                    i_aux = i - 1;
                    j_aux = j + 1;
                    assign_node(grid, node_aux, ind, i_aux, j_aux, grid_x, grid_y);
                    ind++;
                    i_aux = i;
                    j_aux = j - 2;
                    assign_node(grid, node_aux, ind, i_aux, j_aux, grid_x, grid_y);
                    ind++;
                    i_aux = i;
                    j_aux = j - 1;
                    assign_node(grid, node_aux, ind, i_aux, j_aux, grid_x, grid_y);
                    ind++;
                    i_aux = i;
                    j_aux = j;
                    assign_node(grid, node_aux, ind, i_aux, j_aux, grid_x, grid_y);
                    i_aux = i;
                    j_aux = j + 1;
                    assign_node(grid, node_aux, ind, i_aux, j_aux, grid_x, grid_y);
                    ind++;
                    i_aux = i;
                    j_aux = j + 2;
                    assign_node(grid, node_aux, ind, i_aux, j_aux, grid_x, grid_y);
                    ind++;
                    i_aux = i + 1;
                    j_aux = j - 1;
                    assign_node(grid, node_aux, ind, i_aux, j_aux, grid_x, grid_y);
                    ind++;
                    i_aux = i + 1;
                    j_aux = j;
                    assign_node(grid, node_aux, ind, i_aux, j_aux, grid_x, grid_y);
                    ind++;
                    i_aux = i + 1;
                    j_aux = j + 1;
                    assign_node(grid, node_aux, ind, i_aux, j_aux, grid_x, grid_y);
                    ind++;
                    i_aux = i + 2;
                    j_aux = j;
                    assign_node(grid, node_aux, ind, i_aux, j_aux, grid_x, grid_y);
                    ind++;
                    //printf("\n");
                }
            }
            break;
        default:
            printf("Selection type for DPSNN unknown.\n");
            exit(-1);
    }
}

void assign_node(int ** grid, int node_aux, int ind, int ind_x, int ind_y, int grid_x, int grid_y){

    if(ind_x >= 0 && ind_x < grid_x && ind_y >= 0 && ind_y < grid_y){
        grid[node_aux][ind] = (ind_x * grid_x) + ind_y;
        //printf("%d - ",  grid[node_aux][ind]);
    }
}


void lammps(application *app, list_t **task_events, long nodes){

    int i, j;

    long tag = 0;
    int *communicator_0 = malloc(nodes * sizeof(int));
    int comp_base = 100; // usecs
    int data_base = 4; //
    int root_base = 0; //root to bcast
    //computation(app, task_events, communicator_0, nodes, comp_base, comp_base * mod_comp);


    for( i = 0; i < nodes; i++){
        communicator_0[i] = i;
    }

    // Phase 1
    // Init phase
    int mod_data = 1;
    int mod_comp = 1;

    for(i = 1; i < 34; i++){

        if(i % 2 != 0){
            mod_data = 1;
            mod_comp = 2;
        }else{
            mod_data = 5;
            mod_comp = 20;
        }

        computation(app, task_events, communicator_0, nodes, comp_base * mod_comp, comp_base * (mod_comp - 1));

        broadcast(app, task_events, communicator_0, nodes, &tag, root_base, data_base * mod_data);

    }


    mod_comp = 25000; 

    computation(app, task_events, communicator_0, nodes, comp_base * mod_comp, comp_base * (mod_comp - 1000 ));

    allreduce(app, task_events, communicator_0, nodes, &tag, data_base * 2);

    computation(app, task_events, communicator_0, nodes, comp_base * 20, comp_base * 10 );

    allreduce(app, task_events, communicator_0, nodes, &tag, data_base );

    computation(app, task_events, communicator_0, nodes, comp_base * 10, comp_base * 5);

    allreduce(app, task_events, communicator_0, nodes, &tag, data_base * 2);

    computation(app, task_events, communicator_0, nodes, comp_base * 2, comp_base);

    scan(app, task_events, communicator_0, nodes, &tag, data_base * 2);

    computation(app, task_events, communicator_0, nodes, comp_base * 30, comp_base * 20);

    allreduce(app, task_events, communicator_0, nodes, &tag, data_base );

    computation(app, task_events, communicator_0, nodes, comp_base * 2, comp_base);

    allreduce(app, task_events, communicator_0, nodes, &tag, data_base );

    computation(app, task_events, communicator_0, nodes, comp_base * 4, comp_base * 3);


    /***************************************************/

    for(i=0;i<6;i++){

        broadcast(app, task_events, communicator_0, nodes, &tag, root_base, data_base);

        computation(app, task_events, communicator_0, nodes, comp_base * 2, comp_base);

    }	

    broadcast(app, task_events, communicator_0, nodes, &tag, root_base, data_base * 10);

    computation(app, task_events, communicator_0, nodes, comp_base * 40, comp_base * 30);


    mod_comp=50000;

    allreduce(app, task_events, communicator_0, nodes, &tag, data_base * 2);

    computation(app, task_events, communicator_0, nodes, comp_base * mod_comp, comp_base * (mod_comp - 10000));

    allreduce(app, task_events, communicator_0, nodes, &tag, data_base * 2);

    computation(app, task_events, communicator_0, nodes, comp_base * 45, comp_base * 40);

    allreduce(app, task_events, communicator_0, nodes, &tag, data_base * 2);

    computation(app, task_events, communicator_0, nodes, comp_base * 80, comp_base * 70);

    allreduce(app, task_events, communicator_0, nodes, &tag, data_base * 24);

    computation(app, task_events, communicator_0, nodes, comp_base * 80, comp_base * 70);

    allreduce(app, task_events, communicator_0, nodes, &tag, data_base * 2);

    computation(app, task_events, communicator_0, nodes, comp_base * 45, comp_base * 40);


    for(i=0; i<2;i++){
        for(j=0; j<3;j++){
            broadcast(app, task_events, communicator_0, nodes, &tag, root_base, data_base);

            computation(app, task_events, communicator_0, nodes, comp_base * 2, comp_base);
        }

        broadcast(app, task_events, communicator_0, nodes, &tag, root_base, data_base * 5);

        computation(app, task_events, communicator_0, nodes, comp_base * 20, comp_base * 10);

        broadcast(app, task_events, communicator_0, nodes, &tag, root_base, data_base);

        computation(app, task_events, communicator_0, nodes, comp_base * 2, comp_base);

        broadcast(app, task_events, communicator_0, nodes, &tag, root_base, data_base * 8);

        computation(app, task_events, communicator_0, nodes, comp_base * 40, comp_base * 30);

    }

    for(i=0; i<2;i++){
        for(j=0; j<3;j++){
            broadcast(app, task_events, communicator_0, nodes, &tag, root_base, data_base);

            computation(app, task_events, communicator_0, nodes, comp_base * 2, comp_base);
        }

        broadcast(app, task_events, communicator_0, nodes, &tag, root_base, data_base * 5);

        computation(app, task_events, communicator_0, nodes, comp_base * 20, comp_base * 10);

    }

    allreduce(app, task_events, communicator_0, nodes, &tag, data_base);

    computation(app, task_events, communicator_0, nodes, comp_base * 70, comp_base * 60);

    for(i=0;i<4;i++){

        allreduce(app, task_events, communicator_0, nodes, &tag, data_base);

        computation(app, task_events, communicator_0, nodes, comp_base * 2, comp_base);

    }	




    //Phase 2
    //Point to point phase

    long long problem_size = app->pattern_params[0];
    printf("%ld\n",problem_size);
    if(problem_size<55300000000){
        problem_size=55300000000;
        printf("Set problem size in Lammps at 55.3 GB.\n");
    }
    int iterations_base = 1080;
    long long problem_base = 55300000000;
    int n_iterations = ((double)problem_size/(double)problem_base) * iterations_base;
    int msg_size = 50000;

    for(i=0;i<n_iterations;i++){
        for(j=0; j<nodes;j++){
            lammps_p2p_sends(j,app,task_events,communicator_0, &tag, nodes, msg_size);
        }
    }


    //Phase 3
    //AllReduce phase

    for(i = 1; i < 73; i++){

        allreduce(app, task_events, communicator_0, nodes, &tag, data_base);

        computation(app, task_events, communicator_0, nodes, comp_base * 2, comp_base );

    }

    //End Phase



    broadcast(app, task_events, communicator_0, nodes, &tag, root_base, data_base);

    computation(app, task_events, communicator_0, nodes, comp_base * 1500, comp_base * 1000);

    barrier(app, task_events, communicator_0, nodes, &tag);

    // Step 2


    free(communicator_0);

}

void lammps_p2p_sends(int me,application *app,list_t **task_events, int *group,long *tag, int nodes, int data){

    int aux;
    int aux_node;
    long tag_aux;


    tag_aux = *tag;

    //send +- 16 32 64 128 ...
    if(nodes > 16){
        aux_node=me+16;
        if(aux_node<=nodes){
            send(app, task_events, group[me], group[me+16], tag_aux, data);
            receive(app, task_events, group[me], group[me+16], tag_aux++, data);
        }
        aux_node=me-16;
        if(aux_node>0){
            send(app, task_events, group[me], group[me-16], tag_aux, data);
            receive(app, task_events, group[me], group[me-16], tag_aux++, data);
        }
    }	

    if(nodes > 32){
        aux_node=me+32;
        if(aux_node<=nodes){
            send(app, task_events, group[me], group[me+32], tag_aux, data);
            receive(app, task_events, group[me], group[me+32], tag_aux++, data);
        }
        aux_node=me-32;
        if(aux_node>0){
            send(app, task_events, group[me], group[me-32], tag_aux, data);
            receive(app, task_events, group[me], group[me-32], tag_aux++, data);
        }
    }	

    if(nodes > 64){
        aux_node=me+64;
        if(aux_node<=nodes){
            send(app, task_events, group[me], group[me+64], tag_aux, data);
            receive(app, task_events, group[me], group[me+64], tag_aux++, data);
        }	
        aux_node=me-64;
        if(aux_node>0){
            send(app, task_events, group[me], group[me-64], tag_aux, data);
            receive(app, task_events, group[me], group[me-64], tag_aux++, data);
        }
    }	

    if(nodes > 128){
        aux_node=me+128;
        if(aux_node<=nodes){
            send(app, task_events, group[me], group[me+128], tag_aux, data);
            receive(app, task_events, group[me], group[me+128], tag_aux++, data);
        }	
        aux_node=me-128;
        if(aux_node>0){
            send(app, task_events, group[me], group[me-128], tag_aux, data);
            receive(app, task_events, group[me], group[me-128], tag_aux++, data);
        }
    }	

    if((me & 0x11)){
        //send -1 -3
        aux_node=me-1;
        if(aux_node>0){
            send(app, task_events, group[me], group[me-1], tag_aux, data);
            receive(app, task_events, group[me], group[me-1], tag_aux++, data);
        }
        aux_node=me-3;
        if(aux_node>0){
            send(app, task_events, group[me], group[me-3], tag_aux, data);
            receive(app, task_events, group[me], group[me-3], tag_aux++, data);	
        }
    }else{
        if(me & 0x00){
            //send +1 y +3
            aux_node=me+1;
            if(aux_node<=nodes){
                send(app, task_events, group[me], group[me+1], tag_aux, data);
                receive(app, task_events, group[me], group[me+1], tag_aux++, data);	
            }
            aux_node=me+3;
            if(aux_node<=nodes){
                send(app, task_events, group[me], group[me+3], tag_aux, data);
                receive(app, task_events, group[me], group[me+3], tag_aux++, data);	
            }
        }else{ //0x01 or 0x10
            //send +-1
            aux_node=me+1;
            if(aux_node<=nodes){
                send(app, task_events, group[me], group[me+1], tag_aux, data);
                receive(app, task_events, group[me], group[me+1], tag_aux++, data);	
            }
            aux_node=me-1;
            if(aux_node>0){
                send(app, task_events, group[me], group[me-1], tag_aux, data);
                receive(app, task_events, group[me], group[me-1], tag_aux++, data);
            }
        }
    }

    aux = me >> 2;

    if (!aux & 0x11){
        //send +4
        aux_node=me+4;
        if(aux_node<=nodes){
            send(app, task_events, group[me], group[me+4], tag_aux, data);
            receive(app, task_events, group[me], group[me+4], tag_aux++, data);	
        }
    }else{
        if(!aux & 0x00){
            //send -4
            aux_node=me-4;
            if(aux_node>0){
                send(app, task_events, group[me], group[me+4], tag_aux, data);
                receive(app, task_events, group[me], group[me-4], tag_aux++, data);	
            }
        }
    }


    if (aux & 0x00){
        //send +12
        aux_node=me+12;
        if(aux_node<=nodes){
            send(app, task_events, group[me], group[me+12], tag_aux, data);
            receive(app, task_events, group[me], group[me+12], tag_aux++, data);
        }
    }else{
        if(aux & 0x11){
            //send -12
            aux_node=me-12;
            if(aux_node>0){
                send(app, task_events, group[me], group[me-12], tag_aux, data);
                receive(app, task_events, group[me], group[me-12], tag_aux++, data);
            }
        }
    }

    *tag = tag_aux;


}

void regcm(application *app, list_t **task_events, long nodes){

    int i;

    long tag = 0;
    int *communicator_0 = malloc(nodes * sizeof(int));
    int comp_base = 800; // usecs
    int data_base = 8; //
    int root_base = 0; //root to bcast
    //computation(app, task_events, communicator_0, nodes, comp_base, comp_base * mod_comp);


    for( i = 0; i < nodes; i++){
        communicator_0[i] = i;
    }

    // Phase 1

    broadcast(app, task_events, communicator_0, nodes, &tag, root_base, 256);

    computation(app, task_events, communicator_0, nodes, comp_base * 3, comp_base * 2);

    for(i = 1; i < 161; i++){
        broadcast(app, task_events, communicator_0, nodes, &tag, root_base, data_base);

        computation(app, task_events, communicator_0, nodes, comp_base * 3, comp_base);
    }


    computation(app, task_events, communicator_0, nodes, 150000000, 160000000);

    allreduce(app, task_events, communicator_0, nodes, &tag, nodes * 8 );

    for (i = 1 ; i < 2; i++){
        computation(app, task_events, communicator_0, nodes, 900000, 1000000);

        //allgather();
        alltoall(app, task_events, communicator_0, nodes, &tag, nodes * 4 );

    }

    computation(app, task_events, communicator_0, nodes, 900000, 1000000);

    //Phase 2

    comp_base = 500;
    data_base = 400;

    for (i = 1; i < 14348; i++){

        gather(app, task_events, communicator_0, nodes, &tag, root_base, data_base);

        computation(app, task_events, communicator_0, nodes, comp_base * 10, comp_base);

        scatter(app, task_events, communicator_0, nodes, &tag, root_base, data_base);

        computation(app, task_events, communicator_0, nodes, comp_base * 10, comp_base);

    }

    // Step 2


    free(communicator_0);



}


void gadget(application *app, list_t **task_events, long nodes){




}

void lammps2_movement_east_r(application *app, list_t **task_events, int px, int py, int pz, int i, int j, int k, long tag, long data){

    int src, dst;

    src = (pz * py * k) + (py * j) + i;
    dst = (pz * py * k) + (py * j) + i - 1;
    receive(app, task_events, src,  dst, tag, data);
    //              printf("%d -> %d (%ld)\n", src, dst, data);
}

void lammps2_movement_east(application *app, list_t **task_events, int px, int py, int pz, int i, int j, int k, long tag, long data){

    int src, dst;

    src = (pz * py * k) + (py * j) + i;
    dst = (pz * py * k) + (py * j) + i - 1;
    send(app, task_events, src,  dst, tag, data);
    //              printf("%d -> %d (%ld)\n", src, dst, data);
}

void lammps2_movement_west_r(application *app, list_t **task_events, int px, int py, int pz, int i, int j, int k, long tag, long data){

    int src, dst;

    src = (pz * py * k) + (py * j) + i;
    dst = (pz * py * k) + (py * j) + i + 1;
    receive(app, task_events, src,  dst, tag, data);
    //            printf("%d -> %d (%ld)\n", src, dst, data);

}


void lammps2_movement_west(application *app, list_t **task_events, int px, int py, int pz, int i, int j, int k, long tag, long data){

    int src, dst;

    src = (pz * py * k) + (py * j) + i;
    dst = (pz * py * k) + (py * j) + i + 1;
    send(app, task_events, src,  dst, tag, data);
    //            printf("%d -> %d (%ld)\n", src, dst, data);

}

void lammps2_movement_south_r(application *app, list_t **task_events, int px, int py, int pz, int i, int j, int k, long tag, long data){

    int src, dst;

    src = (pz * py * k) + (py * j) + i;
    dst = (pz * py * k) + (py * j) + i + py;
    receive(app, task_events, src,  dst, tag, data);
    //                printf("%d -> %d (%ld)\n", src, dst, data);

}

void lammps2_movement_south(application *app, list_t **task_events, int px, int py, int pz, int i, int j, int k, long tag, long data){

    int src, dst;

    src = (pz * py * k) + (py * j) + i;
    dst = (pz * py * k) + (py * j) + i + py;
    send(app, task_events, src,  dst, tag, data);
    //                printf("%d -> %d (%ld)\n", src, dst, data);

}

void lammps2_movement_north_r(application *app, list_t **task_events, int px, int py, int pz, int i, int j, int k, long tag, long data){

    int src, dst;

    src = (pz * py * k) + (py * j) + i;
    dst = (pz * py * k) + (py * j) + i - py;
    receive(app, task_events, src,  dst, tag, data);
    //                  printf("%d -> %d (%ld)\n", src, dst, data);

}

void lammps2_movement_north(application *app, list_t **task_events, int px, int py, int pz, int i, int j, int k, long tag, long data){

    int src, dst;

    src = (pz * py * k) + (py * j) + i;
    dst = (pz * py * k) + (py * j) + i - py;
    send(app, task_events, src,  dst, tag, data);
    //                  printf("%d -> %d (%ld)\n", src, dst, data);

}

void lammps2_movement_down_r(application *app, list_t **task_events, int px, int py, int pz, int i, int j, int k, long tag, long data){

    int src, dst;

    src = (pz * py * k) + (py * j) + i;
    dst = src + (py * px);
    receive(app, task_events, src,  dst, tag, data);
    //                    printf("%d -> %d (%ld)\n", src, dst, data);

}

void lammps2_movement_down(application *app, list_t **task_events, int px, int py, int pz, int i, int j, int k, long tag, long data){

    int src, dst;

    src = (pz * py * k) + (py * j) + i;
    dst = src + (py * px);
    send(app, task_events, src,  dst, tag, data);
    //                    printf("%d -> %d (%ld)\n", src, dst, data);

}

void lammps2_movement_up_r(application *app, list_t **task_events, int px, int py, int pz, int i, int j, int k, long tag, long data){

    int src, dst;

    src = (pz * py * k) + (py * j) + i;
    dst = src - (py * px);
    receive(app, task_events, src,  dst, tag, data);
    //                    printf("%d -> %d (%ld)\n", src, dst, data);

}

void lammps2_movement_up(application *app, list_t **task_events, int px, int py, int pz, int i, int j, int k, long tag, long data){

    int src, dst;

    src = (pz * py * k) + (py * j) + i;
    dst = src - (py * px);
    send(app, task_events, src,  dst, tag, data);
    //                    printf("%d -> %d (%ld)\n", src, dst, data);

}


void lammps2_pattern1(application *app, list_t **task_events, int px, int py, int pz, long *tag, long atoms_per_proc){

    int i, j ,k;
    long data;

    long tag_aux_s = *tag;
    long tag_aux_r = *tag;

    for(k = 0; k < pz; k++){
        for(j = 0; j < py; j++){
            for(i = 1; i < px; i++){
                data = random() % atoms_per_proc * 4;
                lammps2_movement_east(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data * 8);
            }
        } 
    }

    for(k = 0; k < pz; k++){
        for(j = 0; j < py; j++){
            for(i = 1; i < px; i++){
                data = random() % atoms_per_proc * 4;
                lammps2_movement_east_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data * 8);
            }
        } 
    }

    for(k = 0; k < pz; k++){
        for(j = 0; j < py; j++){
            for(i = 0; i < px -1; i++){
                data = random() % atoms_per_proc * 4;
                lammps2_movement_west(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data * 8);
            }
        } 
    }

    for(k = 0; k < pz; k++){
        for(j = 0; j < py; j++){
            for(i = 0; i < px -1; i++){
                data = random() % atoms_per_proc * 4;
                lammps2_movement_west_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data * 8);
            }
        } 
    }


    for(k = 0; k < pz; k++){
        for(j = 1; j < py ; j++){
            for(i = 0; i < px; i++){
                data = random() % atoms_per_proc * 4;
                lammps2_movement_north(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data * 8);
            }
        } 
    }

    for(k = 0; k < pz; k++){
        for(j = 1; j < py ; j++){
            for(i = 0; i < px; i++){
                data = random() % atoms_per_proc * 4;
                lammps2_movement_north_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data * 8);
            }
        } 
    }

    for(k = 0; k < pz; k++){
        for(j = 0; j < py - 1; j++){
            for(i = 0; i < px; i++){
                data = random() % atoms_per_proc * 4;
                lammps2_movement_south(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data * 8);
            }
        } 
    }

    for(k = 0; k < pz; k++){
        for(j = 0; j < py - 1; j++){
            for(i = 0; i < px; i++){
                data = random() % atoms_per_proc * 4;
                lammps2_movement_south_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data * 8);
            }
        } 
    }

    for(k = 1; k < pz; k++){
        for(j = 0; j < py; j++){
            for(i = 0; i < px; i++){
                data = random() % atoms_per_proc * 4;
                lammps2_movement_up(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data * 8);
            }
        } 
    }

    for(k = 1; k < pz; k++){
        for(j = 0; j < py; j++){
            for(i = 0; i < px; i++){
                data = random() % atoms_per_proc * 4;
                lammps2_movement_up_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data * 8);
            }
        } 
    }

    for(k = 0; k < pz - 1; k++){
        for(j = 0; j < py; j++){
            for(i = 0; i < px; i++){
                data = random() % atoms_per_proc * 4;
                lammps2_movement_down(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data * 8);
            }
        } 
    }

    for(k = 0; k < pz - 1; k++){
        for(j = 0; j < py; j++){
            for(i = 0; i < px; i++){
                data = random() % atoms_per_proc * 4;
                lammps2_movement_down_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data * 8);
            }
        } 
    }


    *tag = tag_aux_s;

}

void lammps2_pattern2(application *app, list_t **task_events, int px, int py, int pz, long *tag, long atoms_per_proc){

    int i, j ,k, d, d2, aux;
    long data, seed;
    char statenew[256];
    char *stateold;
    long tag_aux_s = *tag;
    long tag_aux_r = *tag;


    seed = rand();
    stateold = initstate(seed, statenew, 256);
    for(k = 0; k < pz; k++){
        for(j = 0; j < py; j++){
            for(i = 1; i < px; i++){
                d = random() % 3;
                switch(d){
                    case 0:
                        aux = 0;
                        d2 = (random() % i) + 1;;
                        while(aux <= d2){
                            data = atoms_per_proc * 4 * 8;
                            lammps2_movement_east(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                            aux++;
                        }
                        break;
                    case 1:
                        data = atoms_per_proc * 4 * 8;
                        lammps2_movement_east(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                        break;
                    case 2:
                        data = (random() % atoms_per_proc) * 4 * 8;
                        lammps2_movement_east(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                        break;

                    default:
                        printf("Unkkown type lammps\n");
                        exit(-1);
                }
            }
        } 
    }
    setstate(stateold); 
    stateold = initstate(seed, statenew, 256);
    for(k = 0; k < pz; k++){
        for(j = 0; j < py; j++){
            for(i = 1; i < px; i++){
                d = random() % 3;
                switch(d){
                    case 0:
                        aux = 0;
                        d2 = (random() % i) + 1;;
                        while(aux <= d2){
                            data = atoms_per_proc * 4 * 8;
                            lammps2_movement_east_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                            aux++;
                        }
                        break;
                    case 1:
                        data = atoms_per_proc * 4 * 8;
                        lammps2_movement_east_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                        break;
                    case 2:
                        data = (random() % atoms_per_proc) * 4 * 8;
                        lammps2_movement_east_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                        break;

                    default:
                        printf("Unkkown type lammps\n");
                        exit(-1);
                }
            }
        } 
    }

    setstate(stateold); 

    seed = rand();
    stateold = initstate(seed, statenew, 256);
    for(k = 0; k < pz; k++){
        for(j = 0; j < py; j++){
            for(i = 0; i < px - 1; i++){
                d = random() % 3;
                switch(d){
                    case 0:
                        aux = 0;
                        d2 = (random() % (px - i - 1)) + 1;;
                        while(aux <= d2){
                            data = atoms_per_proc * 4 * 8;
                            lammps2_movement_west(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                            aux++;
                        }
                        break;
                    case 1:
                        data = atoms_per_proc * 4 * 8;
                        lammps2_movement_west(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                        break;
                    case 2:
                        data = (random() % atoms_per_proc) * 4 * 8;
                        lammps2_movement_west(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                        break;

                    default:
                        printf("Unkkown type lammps\n");
                        exit(-1);
                }
            }
        } 
    }
    setstate(stateold); 
    stateold = initstate(seed, statenew, 256);
    for(k = 0; k < pz; k++){
        for(j = 0; j < py; j++){
            for(i = 0; i < px - 1; i++){
                d = random() % 3;
                switch(d){
                    case 0:
                        aux = 0;
                        d2 = (random() % (px - i -1)) + 1;;
                        while(aux <= d2){
                            data = atoms_per_proc * 4 * 8;
                            lammps2_movement_west_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                            aux++;
                        }
                        break;
                    case 1:
                        data = atoms_per_proc * 4 * 8;
                        lammps2_movement_west_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                        break;
                    case 2:
                        data = (random() % atoms_per_proc) * 4 * 8;
                        lammps2_movement_west_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                        break;

                    default:
                        printf("Unkkown type lammps\n");
                        exit(-1);
                }
            }
        } 
    }

    setstate(stateold); 

    seed = rand();
    stateold = initstate(seed, statenew, 256);
    for(k = 0; k < pz; k++){
        for(j = 1; j < py; j++){
            for(i = 0; i < px; i++){
                d = random() % 3;
                switch(d){
                    case 0:
                        aux = 0;
                        d2 = (random() % j) + 1;;
                        while(aux <= d2){
                            data = atoms_per_proc * 4 * 8;
                            lammps2_movement_north(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                            aux++;
                        }
                        break;
                    case 1:
                        data = atoms_per_proc * 4 * 8;
                        lammps2_movement_north(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                        break;
                    case 2:
                        data = (random() % atoms_per_proc) * 4 * 8;
                        lammps2_movement_north(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                        break;

                    default:
                        printf("Unkkown type lammps\n");
                        exit(-1);
                }
            }
        } 
    }
    setstate(stateold); 
    stateold = initstate(seed, statenew, 256);
    for(k = 0; k < pz; k++){
        for(j = 1; j < py; j++){
            for(i = 0; i < px; i++){
                d = random() % 3;
                switch(d){
                    case 0:
                        aux = 0;
                        d2 = (random() % j) + 1;;
                        while(aux <= d2){
                            data = atoms_per_proc * 4 * 8;
                            lammps2_movement_north_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                            aux++;
                        }
                        break;
                    case 1:
                        data = atoms_per_proc * 4 * 8;
                        lammps2_movement_north_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                        break;
                    case 2:
                        data = (random() % atoms_per_proc) * 4 * 8;
                        lammps2_movement_north_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                        break;

                    default:
                        printf("Unkkown type lammps\n");
                        exit(-1);
                }
            }
        } 
    }

    setstate(stateold); 

    seed = rand();
    stateold = initstate(seed, statenew, 256);
    for(k = 0; k < pz; k++){
        for(j = 0; j < py - 1; j++){
            for(i = 0; i < px; i++){
                d = random() % 3;
                switch(d){
                    case 0:
                        aux = 0;
                        d2 = (random() % (py - j - 1)) + 1;;
                        while(aux <= d2){
                            data = atoms_per_proc * 4 * 8;
                            lammps2_movement_south(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                            aux++;
                        }
                        break;
                    case 1:
                        data = atoms_per_proc * 4 * 8;
                        lammps2_movement_south(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                        break;
                    case 2:
                        data = (random() % atoms_per_proc) * 4 * 8;
                        lammps2_movement_south(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                        break;

                    default:
                        printf("Unkkown type lammps\n");
                        exit(-1);
                }
            }
        } 
    }
    setstate(stateold); 
    stateold = initstate(seed, statenew, 256);
    for(k = 0; k < pz; k++){
        for(j = 0; j < py - 1; j++){
            for(i = 0; i < px; i++){
                d = random() % 3;
                switch(d){
                    case 0:
                        aux = 0;
                        d2 = (random() % (py - j - 1)) + 1;;
                        while(aux <= d2){
                            data = atoms_per_proc * 4 * 8;
                            lammps2_movement_south_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                            aux++;
                        }
                        break;
                    case 1:
                        data = atoms_per_proc * 4 * 8;
                        lammps2_movement_south_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                        break;
                    case 2:
                        data = (random() % atoms_per_proc) * 4 * 8;
                        lammps2_movement_south_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                        break;

                    default:
                        printf("Unkkown type lammps\n");
                        exit(-1);
                }
            }
        } 
    }

    seed = rand();
    stateold = initstate(seed, statenew, 256);
    for(k = 1; k < pz; k++){
        for(j = 0; j < py; j++){
            for(i = 0; i < px; i++){
                d = random() % 3;
                switch(d){
                    case 0:
                        aux = 0;
                        d2 = (random() % k) + 1;;
                        while(aux <= d2){
                            data = atoms_per_proc * 4 * 8;
                            lammps2_movement_up(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                            aux++;
                        }
                        break;
                    case 1:
                        data = atoms_per_proc * 4 * 8;
                        lammps2_movement_up(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                        break;
                    case 2:
                        data = (random() % atoms_per_proc) * 4 * 8;
                        lammps2_movement_up(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                        break;

                    default:
                        printf("Unkkown type lammps\n");
                        exit(-1);
                }
            }
        } 
    }
    setstate(stateold); 
    stateold = initstate(seed, statenew, 256);
    for(k = 1; k < pz; k++){
        for(j = 0; j < py; j++){
            for(i = 0; i < px; i++){
                d = random() % 3;
                switch(d){
                    case 0:
                        aux = 0;
                        d2 = (random() % k) + 1;;
                        while(aux <= d2){
                            data = atoms_per_proc * 4 * 8;
                            lammps2_movement_up_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                            aux++;
                        }
                        break;
                    case 1:
                        data = atoms_per_proc * 4 * 8;
                        lammps2_movement_up_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                        break;
                    case 2:
                        data = (random() % atoms_per_proc) * 4 * 8;
                        lammps2_movement_up_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                        break;

                    default:
                        printf("Unkkown type lammps\n");
                        exit(-1);
                }
            }
        } 
    }

    seed = rand();
    stateold = initstate(seed, statenew, 256);
    for(k = 0; k < pz - 1; k++){
        for(j = 0; j < py; j++){
            for(i = 0; i < px; i++){
                d = random() % 3;
                switch(d){
                    case 0:
                        aux = 0;
                        d2 = (random() % (pz - k -1)) + 1;
                        while(aux <= d2){
                            data = atoms_per_proc * 4 * 8;
                            lammps2_movement_down(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                            aux++;
                        }
                        break;
                    case 1:
                        data = atoms_per_proc * 4 * 8;
                        lammps2_movement_down(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                        break;
                    case 2:
                        data = (random() % atoms_per_proc) * 4 * 8;
                        lammps2_movement_down(app, task_events, px, py, pz, i, j, k, tag_aux_s++, data);
                        break;

                    default:
                        printf("Unkkown type lammps\n");
                        exit(-1);
                }
            }
        } 
    }
    setstate(stateold); 
    stateold = initstate(seed, statenew, 256);
    for(k = 0; k < pz - 1; k++){
        for(j = 0; j < py; j++){
            for(i = 0; i < px; i++){
                d = random() % 3;
                switch(d){
                    case 0:
                        aux = 0;
                        d2 = (random() % (pz - k -1)) + 1;
                        while(aux <= d2){
                            data = atoms_per_proc * 4 * 8;
                            lammps2_movement_down_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                            aux++;
                        }
                        break;
                    case 1:
                        data = atoms_per_proc * 4 * 8;
                        lammps2_movement_down_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                        break;
                    case 2:
                        data = (random() % atoms_per_proc) * 4 * 8;
                        lammps2_movement_down_r(app, task_events, px, py, pz, i, j, k, tag_aux_r++, data);
                        break;

                    default:
                        printf("Unkkown type lammps\n");
                        exit(-1);
                }
            }
        } 
    }

    setstate(stateold); 
    *tag = tag_aux_s;
}

void lammps2(application *app, list_t **task_events, long nodes){

    int i;
    int phases, px, py, pz;
    long atoms, atoms_per_proc, tag;

    phases = app->pattern_params[0]; 
    atoms = app->pattern_params[1]; 
    px  = app->pattern_params[2];
    py = app->pattern_params[3];
    pz = app->pattern_params[4];

    tag = 0;

    atoms_per_proc = atoms / nodes;

    for(i = 0; i < phases; i++){
        lammps2_pattern1(app, task_events, px, py, pz, &tag, atoms_per_proc);

        lammps2_pattern2(app, task_events, px, py, pz, &tag, atoms_per_proc);
    }


    //printf("(%ld, %ld, %ld) %ld %ld\n", px, py, pz, atoms_per_proc, atoms);

}

void regcm2(application *app, list_t **task_events, long nodes){

    int i, j, k;
    int phases, px, py, dist;
    long tag_s, tag_r, size, src, dst;
    long seed, aux, size_diag;
    char statenew[256];
    char *stateold;
    long size_io = 24000000;
    int *communicator_0 = malloc(nodes * sizeof(int));
    
    for(i = 0; i < nodes; i++){
        communicator_0[i] = i;
    }
    

    phases = app->pattern_params[0]; 
    px  = app->pattern_params[1];
    py = app->pattern_params[2];
    size =  app->pattern_params[3];

    tag_s = 0;
    tag_r = 0;
    size_diag = 8;

    if ((dist=(long)sqrt(nodes))!=(long)ceil(sqrt(nodes))){
        printf("Number of Nodes is not a quadratic value\n");
        exit(0);
    }

    for (k = 0; k < phases; k++) {
        tag_r = tag_s;
        for (i=0; i<nodes; i++){
            if ((i%dist)<(dist-1))  // Sends to +1
                send(app, task_events, i, i + 1, tag_s++, size);
            if ((i%dist)!=0)        // Sends to -1
                send(app, task_events, i, i - 1, tag_s++, size);
            if ((i/dist)<(dist-1))  // Sends to +dist
                send(app, task_events, i, i + dist, tag_s++, size);
            if ((i/dist)!=0)        // Sends to -dist
                send(app, task_events, i, i - dist, tag_s++, size);
        }

        for (i=0; i<nodes; i++){
            if ((i%dist)<(dist-1))  // Waits for +1
                receive(app, task_events, i, i + 1, tag_r++, size);
            if ((i%dist)!=0)        // Waits for -1
                receive(app, task_events, i, i - 1, tag_r++, size);
            if ((i/dist)<(dist-1))  // Waits for +dist
                receive(app, task_events, i, i + dist, tag_r++, size);
            if ((i/dist)!=0)        // Waits for -dist
                receive(app, task_events, i, i - dist, tag_r++, size);  
        }
        seed = rand();
        stateold = initstate(seed, statenew, 256);

        for(j = 0; j < py; j++){
            for (i = 0; i < px; i++){
                src = ((j * py) + i);
                aux = random() % 2;
                    if(aux == 0){
                        if(i > 0 && j > 0){
                            dst = (((j - 1) * py)) + (i -1); 
                            send(app, task_events, src, dst, tag_s++, size_diag); 
                        }
                        if(i > 0 && j < py - 1){
                            dst = (((j + 1) * py)) + (i - 1); 
                            send(app, task_events, src, dst, tag_s++, size_diag); 
                        }
                        if(i < px - 1 && j > 0){
                            dst = (((j - 1) * py)) + (i + 1); 
                            send(app, task_events, src, dst, tag_s++, size_diag); 
                        }
                        if(i < px - 1 && j < py - 1){
                            dst = (((j + 1) * py)) + (i + 1); 
                            send(app, task_events, src, dst, tag_s++, size_diag); 
                        }

                    }
            }


        }
        setstate(stateold); 
        stateold = initstate(seed, statenew, 256);

        for(j = 0; j < py; j++){
            for (i = 0; i < px; i++){
                src = ((j * py) + i);
                aux = random() % 2;
                    if(aux == 0){
                        if(i > 0 && j >0){
                            dst = (((j-1) * py)) + (i -1); 
                            receive(app, task_events, src, dst, tag_r++, size_diag); 
                        }
                    if(i > 0 && j < py - 1){
                            dst = (((j + 1) * py)) + (i - 1); 
                            receive(app, task_events, src, dst, tag_r++, size_diag); 
                        }
                        if(i < px - 1 && j > 0){
                            dst = (((j - 1) * py)) + (i + 1); 
                            receive(app, task_events, src, dst, tag_r++, size_diag); 
                        }

                        if(i < px - 1 && j < py - 1){
                            dst = (((j + 1) * py)) + (i  + 1); 
                            receive(app, task_events, src, dst, tag_r++, size_diag); 
                        }
            }


        }
        }
        setstate(stateold); 
 
        alltoone(app, task_events, communicator_0, nodes, &tag_s, 0, size_io);
    }
    free(communicator_0);
}





























