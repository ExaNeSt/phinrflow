#ifndef _neighbours
#define _neighbours

void mesh2d_woc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases);

void mesh2d_wc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases);

void mesh3d_woc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases);

void mesh3d_wc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases);

void torus2d_woc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases);

void torus3d_woc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases);

void torus2d_wc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases);

void torus3d_wc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases);

void waterfall(application *app, list_t **task_events, long nodes, long size, long computation, long phases);

void binarytree(application *app, list_t **task_events, long nodes, long size, long computation, long phases);

void butterfly(application *app, list_t **task_events, long nodes, long size, long computation, long phases);

#endif
