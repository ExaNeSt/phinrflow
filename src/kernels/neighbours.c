#include "../phinrflow/gen_trace.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Prints a trace in the standard output moving in a 2D Mesh towards X & Y.
 *
 *@param nodes   The number of nodes. It should be a quadratic value, if not an error is printed in the error output.
 *@param size            The size of message.
 */
void mesh2d_woc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases)
{
    long dist;      ///< nodes ^ 1/2
    long i,j;         ///< The number of the node.
    long tag_s = 0;       ///< The tag
    long tag_r = 0;       ///< The tag

    if ((dist=(long)sqrt(nodes))!=(long)ceil(sqrt(nodes))){
        printf("Number of Nodes is not a quadratic value\n");
        exit(0);
    }
    else
    {
    	if(nphases < 1)
			nphases = 1;

        for (j=0; j<nphases; j++) {
            for (i=0; i<nodes; i++){
                if ((i%dist)<(dist-1))  // Sends to +1
                    send(app, task_events, i, i + 1, tag_s++, size);
                if ((i%dist)!=0)        // Sends to -1
                    send(app, task_events, i, i - 1, tag_s++, size);
                if ((i/dist)<(dist-1))  // Sends to +dist
                    send(app, task_events, i, i + dist, tag_s++, size);
                if ((i/dist)!=0)        // Sends to -dist
                    send(app, task_events, i, i - dist, tag_s++, size);
            }

            for (i=0; i<nodes; i++){
                if ((i%dist)<(dist-1))  // Waits for +1
                    receive(app, task_events, i, i + 1, tag_r++, size);
                if ((i%dist)!=0)        // Waits for -1
                    receive(app, task_events, i, i - 1, tag_r++, size);
                if ((i/dist)<(dist-1))  // Waits for +dist
                    receive(app, task_events, i, i + dist, tag_r++, size);
                if ((i/dist)!=0)        // Waits for -dist
                    receive(app, task_events, i, i - dist, tag_r++, size);  
            }
        }
    }
}

/**
 * Prints a trace in the standard output moving in a 2D Mesh towards X & Y.
 *
 *@param nodes   The number of nodes. It should be a quadratic value, if not an error is printed in the error output.
 *@param size            The size of message.
 */
void mesh2d_wc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases)
{
    long dist;      ///< nodes ^ 1/2
    long i,j;         ///< The number of the node.
    long tag_s = 0;       ///< The tag
    long tag_r = 0;       ///< The tag

    if ((dist=(long)sqrt(nodes))!=(long)ceil(sqrt(nodes))){
        printf("Number of Nodes is not a quadratic value\n");
        exit(0);
    }
    else
    {
    	if(nphases < 1)
			nphases = 1;

        for (j=0; j<nphases; j++) {
            for (i=0; i<nodes; i++){
                if ((i%dist)<(dist-1))  // Sends to +1
                    send(app, task_events, i, i + 1, tag_s++, size);
                if ((i/dist)<(dist-1))  // Sends to +dist
                    send(app, task_events, i, i + dist, tag_s++, size);

                if ((i%dist)<(dist-1))  // +X waits for i
                    receive(app, task_events, i, i + 1, tag_r++, size);
                if ((i/dist)<(dist-1))  // +Y waits for i
                    receive(app, task_events, i, i + dist, tag_r++, size);
            }
        }
    }
}

void mesh3d_woc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases)
{
    long i,j;         ///< The number of the node.
    long dist;      ///< nodes ^ 1/3
    long dist2;     ///< nodes ^ 2/3
    long tag_s = 0;       ///< The tag
    long tag_r = 0;       ///< The tag


    dist=(long)pow(nodes, 1.0/3);
    while(dist*dist*dist<nodes)
        dist++;
    if (dist*dist*dist!=nodes){
        printf("Number of Nodes is not a cubic value.\n");
        exit(0);
    }
    else
    {
    	if(nphases < 1)
			nphases = 1;

        dist2=dist*dist;
        for (j=0; j<nphases; j++) {
            for (i=0; i<nodes; i++){
                if (i%dist<dist-1)              // Sends to +X
                    send(app, task_events, i, i + 1, tag_s++, size);
                if (i%dist)                     // Sends to -X
                    send(app, task_events, i, i - 1, tag_s++, size);
                if ((i/dist)%dist<dist-1)       // Sends to +Y
                    send(app, task_events, i, i + dist, tag_s++, size);
                if ((i/dist)%dist)              // Sends to -Y
                    send(app, task_events, i, i - dist, tag_s++, size);
                if (i/dist2<dist-1)             // Sends for +Z
                    send(app, task_events, i, i + dist2, tag_s++, size);
                if (i/dist2)                    // Sends to -Z
                    send(app, task_events, i, i - dist2, tag_s++, size);
            }

            for (i=0; i<nodes; i++){
                if (i%dist<dist-1)              // +X waits for i
                    receive(app, task_events, i, i + 1, tag_r++, size);
                if (i%dist)                     // -X waits for i
                    receive(app, task_events, i, i - 1, tag_r++, size);
                if ((i/dist)%dist<dist-1)       // +Y waits for i
                    receive(app, task_events, i, i + dist, tag_r++, size);
                if ((i/dist)%dist)              // -Y waits for i
                    receive(app, task_events, i, i - dist, tag_r++, size);
                if (i/dist2<dist-1)             // +Z waits for i
                    receive(app, task_events, i, i + dist2, tag_r++, size);
                if (i/dist2)                    // -Z waits for i
                    receive(app, task_events, i, i - dist2, tag_r++, size);
            }
        }
    }
}

void mesh3d_wc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases)
{
    long i,j;         ///< The number of the node.
    long dist;      ///< nodes ^ 1/3
    long dist2;     ///< nodes ^ 2/3
    long tag_s = 0;       ///< The tag
    long tag_r = 0;       ///< The tag


    dist=(long)pow(nodes, 1.0/3);
    while(dist*dist*dist<nodes)
        dist++;
    if (dist*dist*dist!=nodes){
        printf("Number of Nodes is not a cubic value.\n");
        exit(0);
    }
    else
    {
    	if(nphases < 1)
			nphases = 1;

        dist2=dist*dist;
        for (j=0; j<nphases; j++) {
            for (i=0; i<nodes; i++){
                if (i%dist<dist-1)              // Sends to +X
                    send(app, task_events, i, i + 1, tag_s++, size);
                if ((i/dist)%dist<dist-1)       // Sends to +Y
                    send(app, task_events, i, i + dist, tag_s++, size);
                if (i/dist2<dist-1)             // Sends for +Z
                    send(app, task_events, i, i + dist2, tag_s++, size);

                if (i%dist<dist-1)              // +X waits for i
                    receive(app, task_events, i, i + 1, tag_r++, size);
                if ((i/dist)%dist<dist-1)       // +Y waits for i
                    receive(app, task_events, i, i + dist, tag_r++, size);
                if (i/dist2<dist-1)             // +Z waits for i
                    receive(app, task_events, i, i + dist2, tag_r++, size);
            }
        }
    }
}

/**
 * Prints a trace in the standard output moving in a 2D Mesh towards X & Y.
 *
 *@param nodes   The number of nodes. It should be a quadratic value, if not an error is printed in the error output.
 *@param size            The size of message.
 */
void torus2d_woc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases){

    long dist;      ///< nodes ^ 1/2
    long i, j;         ///< The number of the node.
    long x,y;       ///< The coordinates of the node

    if ((dist=(long)sqrt(nodes))!=(long)ceil(sqrt(nodes))){
        printf("Number of Nodes is not a quadratic value\n");
        exit(-1);
    }
    else
    {
    	if(nphases < 1)
			nphases = 1;

        for (j = 0; j < nphases; j++) {
            for (i = 0; i < nodes; i++){
				x=i%dist;
                y=i/dist;
				send(app, task_events, i, ((x + 1) % dist)+(y*dist), j, size);
				send(app, task_events, i, ((x + dist - 1) % dist)+(y*dist), j, size);
				send(app, task_events, i, x + (((y + 1) % dist)*dist), j, size);
				send(app, task_events, i, x + (((y + dist - 1) % dist)*dist), j, size);
            }
            for (i = 0; i < nodes; i++){
				x=i%dist;
                y=i/dist;
				receive(app, task_events, i, ((x + 1) % dist)+(y*dist), j, size);
				receive(app, task_events, i, ((x + dist - 1) % dist)+(y*dist), j, size);
				receive(app, task_events, i, x + (((y + 1) % dist)*dist), j, size);
				receive(app, task_events, i, x + (((y + dist - 1) % dist)*dist), j, size);
            }
        }
    }
}

/**
 * Prints a trace in the standard output moving in a 2D Mesh towards X & Y.
 *
 *@param nodes   The number of nodes. It should be a quadratic value, if not an error is printed in the error output.
 *@param size            The size of message.
 */
void torus2d_wc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases){

    long dist;      ///< nodes ^ 1/2
    long i, j, t, n;         ///< The number of the node , the number of phase, the position of the tail of queue, and the neighbour node.
    long x,y;       ///< The coordinates of the node
    long *visited;	///< how many times a node has been visited (should be 4 at the end of the simulation)
    long *breadth_order;	///< a static implementation of a queue to which newly visited nodes are added to flood the system in order

    if ((dist=(long)sqrt(nodes))!=(long)ceil(sqrt(nodes))){
        printf("Number of Nodes is not a quadratic value\n");
        exit(-1);
    }
    else
    {
    	if(nphases < 1)
			nphases = 1;

    	visited=malloc(nodes*sizeof(long));
    	breadth_order=malloc(nodes*sizeof(long));
    	for (j = 0; j < nphases; j++) {
			breadth_order[0]=0; // We could also start from a random node
			t=1; // The index of the breadth order queue tail
			visited[0]=1;
			for (i = 1; i < nodes; i++)
				visited[i]=0;

            for (i = 0; i < nodes; i++){ /// TODO: i should not go from 0..N^2, but has to go "around" the origin in x+, x-, y+, y-
				x=breadth_order[i]%dist;
                y=breadth_order[i]/dist;
                printf("visit node %ld (%ld, %ld)\n",breadth_order[i], x, y);

                // X+
                n=((x + 1) % dist)+(y*dist);
				if (visited[n]++ == 0)
					breadth_order[t++]=n;
				send(app, task_events, i, n, j, size);
				receive(app, task_events, i, n, j, size);

				// X-
                n=((x + dist - 1) % dist)+(y*dist);
				if (visited[n]++ == 0)
					breadth_order[t++]=n;
				send(app, task_events, i, n, j, size);
				receive(app, task_events, i, n, j, size);

				// Y+
                n=x + (((y + 1) % dist)*dist);
				if (visited[n]++ == 0)
					breadth_order[t++]=n;
				send(app, task_events, i, n, j, size);
				receive(app, task_events, i, n, j, size);

				// Y-
                n=x + (((y + dist - 1) % dist)*dist);
				if (visited[n]++ == 0)
					breadth_order[t++]=n;
				send(app, task_events, i, n, j, size);
				receive(app, task_events, i, n, j, size);
            }
        }
        free(visited);
        free(breadth_order);
    }
}

void torus3d_woc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases){

    long i, j;         ///< The number of the node.
    long dist;      ///< nodes ^ 1/3
    long dist2;     ///< nodes ^ 2/3
    long x,y,z;       ///< The coordinates of the node

    dist = (long)pow(nodes, 1.0 / 3);
    while(dist * dist * dist < nodes)
        dist++;
    if (dist * dist * dist != nodes){
        printf("Number of Nodes is not a cubic value.\n");
        exit(0);
    }
    else
    {
    	if (nphases<1)
            nphases=1;

        dist2 = dist * dist;
        for (j = 0; j < nphases; j++){
            for (i = 0; i < nodes; i++){
				x=i%dist;
                y=(i/dist)%dist;
                z=i/dist2;
				send(app, task_events, i, ((x + 1) % dist) + (y*dist) + (z*dist2), j, size);
				send(app, task_events, i, ((x + dist - 1) % dist) + (y*dist) + (z*dist2), j, size);
				send(app, task_events, i, x + (((y + 1) % dist)*dist) + (z*dist2), j, size);
				send(app, task_events, i, x + (((y + dist - 1) % dist)*dist) + (z*dist2), j, size);
				send(app, task_events, i, x + (y*dist) + (((z + 1) % dist)*dist2), j, size);
				send(app, task_events, i, x + (y*dist) + (((z + dist - 1) % dist)*dist2), j, size);
            }
            for (i = 0; i < nodes; i++){
				x=i%dist;
                y=(i/dist)%dist;
                z=i/dist2;
				receive(app, task_events, i, ((x + 1) % dist) + (y*dist) + (z*dist2), j, size);
				receive(app, task_events, i, ((x + dist - 1) % dist) + (y*dist) + (z*dist2), j,  size);
				receive(app, task_events, i, x + (((y + 1) % dist)*dist) + (z*dist2), j, size);
				receive(app, task_events, i, x + (((y + dist - 1) % dist)*dist) + (z*dist2), j, size);
				receive(app, task_events, i, x + (y*dist) + (((z + 1) % dist)*dist2), j, size);
				receive(app, task_events, i, x + (y*dist) + (((z + dist - 1) % dist)*dist2), j, size);
            }
        }
    }
}

void torus3d_wc(application *app, list_t **task_events, long nodes, long size, long computation, long nphases){

    long i, j, t, n;         ///< The number of the node.
    long dist;      ///< nodes ^ 1/3
    long dist2;     ///< nodes ^ 2/3
    long x, y, z;
    long *visited;	///< how many times a node has been visited (should be 6 at the end of the simulation)
	long *breadth_order;	///< a static implementation of a queue to which newly visited nodes are added to flood the system in order

    dist = (long)pow(nodes, 1.0 / 3);
    while(dist * dist * dist < nodes)
        dist++;
    if (dist * dist * dist != nodes){
        printf("Number of Nodes is not a cubic value.\n");
        exit(0);
    }
    else
    {
    	if (nphases<1)
            nphases=1;
        dist2 = dist * dist;

    	visited=malloc(nodes*sizeof(long));
    	breadth_order=malloc(nodes*sizeof(long));

        for (j = 0; j < nphases; j++){
        	breadth_order[0]=0; // We could also start from a random node
			t=1; // The index of the breadth order queue tail
			visited[0]=1;
			for (i = 1; i < nodes; i++)
				visited[i]=0;

            for (i = 0; i < nodes; i++){
				x=breadth_order[i]%dist;
                y=(breadth_order[i]/dist)%dist;
                z=breadth_order[i]/dist2;
                //printf("visit node %ld (%ld, %ld, %ld)\n",breadth_order[i], x, y,z);

				// X+
                n=((x + 1) % dist)+(y*dist)+(z*dist2);
				if (visited[n]++ == 0)
					breadth_order[t++]=n;
				send(app, task_events, i, n, j, size);
                receive(app, task_events, i, n, j, size);

				// X-
                n=((x + dist - 1) % dist)+(y*dist)+(z*dist2);
				if (visited[n]++ == 0)
					breadth_order[t++]=n;
				send(app, task_events, i, n, j, size);
                receive(app, task_events, i, n, j, size);

				// Y+
                n= x + (((y + 1) % dist)*dist)+(z*dist2);
				if (visited[n]++ == 0)
					breadth_order[t++]=n;
				send(app, task_events, i, n, j, size);
                receive(app, task_events, i, n, j, size);

				// Y-
                n=x + (((y + dist - 1) % dist)*dist)+(z*dist2);
				if (visited[n]++ == 0)
					breadth_order[t++]=n;
				send(app, task_events, i, n, j, size);
                receive(app, task_events, i, n, j, size);

                // Z+
                n= x +(y*dist) + (((z + 1) % dist)*dist2);
				if (visited[n]++ == 0)
					breadth_order[t++]=n;
				send(app, task_events, i, n, j, size);
                receive(app, task_events, i, n, j, size);

				// Z-
                n= x +(y*dist) + (((z + dist - 1) % dist)*dist2);
				if (visited[n]++ == 0)
					breadth_order[t++]=n;
				send(app, task_events, i, n, j, size);
                receive(app, task_events, i, n, j, size);
            }
        }
        free(visited);
        free(breadth_order);
    }
}

/**
 * Prints a trace in the standard output similar to the waterfalls in LU.
 *
 *@param nodes   The number of nodes. It should be a quadratic value, if not an error is printed in the error output.
 *@param size    The size of the burst.
 */
void waterfall(application *app, list_t **task_events, long nodes, long size, long computation, long phases)
{
    long i,j;                                 ///< The number of the node.
    long dist;      ///< nodes ^ 1/2
    long n;                                 ///< The total amount of bytes sent
    long its = 1;

    if ((dist=(long)sqrt(nodes))!=(long)ceil(sqrt(nodes))){
        printf("Number of Nodes is not a quadratic value\n");
        exit(-1);
    }
    else
    {
    	if (phases<1)
            phases=1;

        for (j = 0; j < phases; j++){
            for (i = 0; i < nodes; i++)
                for (n = 0;  n < its; n++)
                {
                    if (i % dist)             // Waits for -1
                        receive(app, task_events, i - 1, i, n, size);
                    if (i/dist)             // Waits for -dist
                        receive(app, task_events, i - dist, i, n, size);
                    if (i%dist<dist-1)      // Sends to +1
                        send(app, task_events, i, i + 1, n, size);
                    if (i/dist<dist-1)      // Sends to +dist
                        send(app, task_events, i, i + dist, n, size);
                }
        }
    }
}

/**
 * Prints a trace in the standard output performing a binary tree interchange. (Reduce)
 *
 *@param nodes	The number of nodes. It should be a power of two value, if not an error is printed in the error output.
 *@param size	The size of the burst.
 */
void binarytree(application *app, list_t **task_events, long nodes, long size, long computation, long phases)
{
    long i;
    long N=1;	///< 2 ^ nodes 
    long k=2;		///< 2 ^ N
    long **tags;
    long l;
    long n;					///< The iteration.
    long pn, pn1;				///< 2 ^ n & 2 ^ (n+1)


    tags = malloc(nodes * sizeof(long*));
    for(i = 0; i < nodes;i++){
        tags[i] = calloc(nodes, sizeof(long));
    }

    while (k<nodes)
    {	
        N++;
        k=2*k;
    }

    if (nodes!=k)
        perror("Number of Nodes is not a power of two\n");
    else
    {
    	if (phases<1)
            phases=1;

        for (l=0; l<phases; l++){
            for (i=0; i<nodes; i++)
            {
                pn=1;
                pn1=2;
                for (n=0; n<N; n++)
                {
                    if ((i%pn1) && !(i % pn))
                        send(app, task_events, i, i - pn, tags[i][i - pn], size);
                    if ( !(i%pn1) )
                        receive(app, task_events, i + pn, i, tags[i][i + pn]++, size);
                    pn=pn1;
                    pn1=2*pn1;
                }
            }
        }
    }

    for(i = 0; i < nodes;i++){
        free(tags[i]);
    }
    free(tags);
}

/**
 * Prints a trace in the standard output performing a butterfly interchange. (allReduce)
 * For each level of the butterfly the size of the message is reduced.
 *
 *@param nodes	The number of nodes. It should be a power of two value, if not an error is printed in the error output.
 *@param size	The size of the burst.
 */
void butterfly(application *app, list_t **task_events, long nodes, long size, long computation, long phases)
{
    long N=0; ///< log2 nodes
    long k=1; ///< 2 ^ N
    long tag_s = 0;
    long tag_r = 0;
    long l;
    long i;					///< The number of the node.
    long n;
    long pn, pn1;				///< 2 ^ n & 2 ^ (n+1)
        
    while (k<nodes){
        N++;
        k = 2 * k;
    }

    if (nodes != k){
        printf("Number of Nodes is not a power of two\n");
        exit(-1);
    }
    else
    {
    	if (phases<1)
            phases=1;

        for (l = 0; l < phases; l++){
            for (i = 0; i < nodes; i++){
                pn = 1;
                pn1 = 2;
                for(n = 0; n < N; n++)
                {
                    if (i % pn1 < pn)
                    {
                        send(app, task_events, i, i + pn, tag_s++, size);
                        receive(app, task_events, i, i + pn, tag_r++, size);
                    }
                    else
                    {
                        send(app, task_events, i, i - pn, tag_s++, size);
                        receive(app, task_events, i, i - pn, tag_r++, size);
                    }
                    pn = pn1;
                    pn1 = 2 * pn1;
                }
            }
        }
    }
}
