#ifndef _pseudoapps
#define _pseudoapps

void gups(application *app, list_t **task_events, long nodes, long size, long computation, long phases);

void nbodies(application *app, list_t **task_events, long nodes, long size, long computation, long phases);

void shift(application *app, list_t **task_events, long nodes, long size, long computation, long phases);

void bisection(application *app, list_t **task_events, long nodes, long size, long computation, long phases);

void hotregion(application *app, list_t **task_events, long nodes, long size, long computation, long phases);

void mapreduce(application *app, list_t **task_events, long nodes, long size, long computation, long phases);

#endif
