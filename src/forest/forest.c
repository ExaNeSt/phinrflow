/** @mainpage
  forest topology <stages, (ports_down, ports_up)^stages>
  */

#include <stdlib.h>
#include <stdio.h>

#include "../phinrflow/node.h"
#include "../phinrflow/misc.h"
#include "../phinrflow/globals.h"
#include "forest.h"

forest_t forest;

static char* network_token="forest";
static char routing_token[20];
static char* topo_version="v0.1";
static char* topo_param_tokens[11]= {"trees","stages","down0","up0","down1","up1","down2","up2","down3","up3","down4","up4"};
//AE: make error checking so that we don't overrun this buffer
extern char filename_params[100];
static char *routing_param_tokens[1]= {"max_paths"};


/**
 * Initializes the topology and sets the parameters k & n.
 */
long init_topo_forest(long np, long* par)
{
    long i, j, k, c;
    long buffer_length;

    if (np < 1) {
        printf("Parameters needed\n");
        exit(-1);
    }
    forest.param_trees = par[0];
    forest.param_k = par[1];

    if(forest.param_trees < 1){
        printf("Positive number of trees needed\n");
        exit(-1);
    }
    if(forest.param_k > 3){
        printf("number of stages limited to 3\n");
        exit(-1);
    }

    c = 2;
    forest.tree = malloc(forest.param_trees * sizeof(struct tree_t));
    buffer_length = sprintf(filename_params, "t%ldk%ld", forest.param_trees, forest.param_k);
    for(i = 0; i < forest.param_trees; i++){
        forest.tree[i].param_down = malloc(forest.param_k * sizeof(long));
        forest.tree[i].param_up = malloc(forest.param_k * sizeof(long));
        for (j = 0;j < forest.param_k; j++){
            forest.tree[i].param_down[j] = par[c++];
            forest.tree[i].param_up[j] = par[c++];
            buffer_length += sprintf(filename_params + buffer_length, "d%ldu%ld", forest.tree[i].param_down[i], forest.tree[i].param_up[i]);
        }
    }

    for (i = 0;i < forest.param_trees; i++){
        forest.tree[i].up_pow = malloc((forest.param_k + 1) * sizeof(long));
        forest.tree[i].down_pow = malloc((forest.param_k + 1) * sizeof(long));
        forest.tree[i].cur_route=malloc(2 * forest.param_k * sizeof(long));   // UP*/DOWN routes cannot be longer than 2*k
        forest.tree[i].sw_per_stage=malloc(forest.param_k*sizeof(long));
        forest.tree[i].down_pow[0]=1;
        forest.tree[i].up_pow[0]=1;
        for (j = 0; j < forest.param_k; j++) {
            forest.tree[i].down_pow[j + 1]=forest.tree[i].down_pow[j] * forest.tree[i].param_down[j];	// product of param_down[i] for 0<=i<n
            forest.tree[i].up_pow[j + 1]=forest.tree[i].up_pow[j] * forest.tree[i].param_up[j];			// product of param_up[i]   for 0<=i<n
        } // numbers of up and down ports will be useful throughout,so let's compute them just once.
    }


    switches=0;
    for (i = 0;i < forest.param_trees; i++){
        for (j = 0;j < forest.param_k; j++) {
            forest.tree[i].sw_per_stage[j] =  forest.tree[i].up_pow[j];
            for (k = j + 1;k < forest.param_k;k++){
                forest.tree[i].sw_per_stage[j] *= forest.tree[i].param_down[k];
            }
            forest.tree[i].switches += forest.tree[i].sw_per_stage[j];
            forest.tree[i].ports += forest.tree[i].sw_per_stage[j]*forest.tree[i].param_down[j];
        }// number of switches per stage will be useful throughout, so let's compute them just once.
    }
    forest.servers = forest.tree[i].down_pow[forest.param_k];

    switch(routing){
        case TREE_STATIC_ROUTING:
            snprintf(routing_token,20,"tree-static");
            break;
        case TREE_RND_ROUTING:
            snprintf(routing_token,20,"tree-random");
            break;
        case TREE_RR_ROUTING:
            snprintf(routing_token,20,"tree-roundrobin");
            for (i = 0;i < forest.param_trees; i++){
                forest.tree[i].path_index = malloc(servers * sizeof(long));
                for (j = 0;j < servers; j++){
                    forest.tree[i].path_index[j] = j;
                }
            }
            break;
        default:
            printf("Not a tree-compatible routing %d", routing);
            exit(-1);
    }

    if(routing_nparam > 0){
        for (i = 0;i < forest.param_trees; i++){
            forest.tree[i].max_paths = routing_params[0];
        }
    } 
    else{
        for (i = 0;i < forest.param_trees; i++){
            forest.tree[i].max_paths = 1;
        }
    }

    return 1; //Return status, not used here.
}

/**
 * Release the resources used by the topology.
 **/
void finish_topo_forest(){

    int i;

    for (i = 0;i < forest.param_trees; i++){
        free(forest.tree[i].cur_route);
        free(forest.tree[i].up_pow);
        free(forest.tree[i].down_pow);
        free(forest.tree[i].sw_per_stage);
        free(forest.tree[i].param_down);
        free(forest.tree[i].param_up);
        if (forest.tree[i].path_index != NULL)
            free(forest.tree[i].path_index);
    }
    free(forest.tree);
}

/**
 * Get the number of servers of the network
 */
long get_servers_forest(){

    return servers;
}

/**
 * Get the number of switches of the network
 */
long get_switches_forest(){

    return switches;
}

/**
 * Get the number of ports of a given node (either a server or a switch, see above)
 */
long get_radix_forest(long n){

    int i = 0;

    if (n < servers){
        return(forest.param_trees);	// If this is a server it has 1 port
    }
    else{
        n -= servers;
        while( n>= sw_per_stage[i]){
            n -= sw_per_stage[i];
            i++;
        }
    }
    return param_down[i]+param_up[i]; // If this is a switch the number of ports depends on the stage
}

/**
 * Calculates connections
 */
tuple_t connection_forest(long node, long port){

    tuple_t res;
    long tree_id, lvl, pos, nl_first, p; // id of the first switch in the NEIGHBOURING level

    if (node < servers){
        if (port > forest.param_trees) {
            res.node =- 1; // switch 'sw' in level 'port'
            res.port =- 1;
        } 
        else{
            res.node = servers + (node / forest.tree[0].param_down[0]);
            res.port = (node % forest.tree[0].param_down[0]);
        }
    }
    else{
        tree_id = (node - servers) / forest.param_trees;
        pos = (node - servers);
        lvl = 0;
        nl_first = servers;
        while (pos >= forest.treesw_per_stage[lvl]){
            pos -= sw_per_stage[lvl];
            nl_first += sw_per_stage[lvl];
            lvl++;
        }
        if(lvl == param_k - 1 && port >= param_down[lvl]) {   //disconnected links in the last stage of the forest (param_up[last] should be 0 any way!!!)
            res.node =- 1;
            res.port =- 1;
        } 
        else if (lvl == 0 && port < param_down[lvl]) { // connected to server
            res.node = port + (pos * down_pow[1]);
            res.port = 0;
        } 
        else if (port >= param_down[lvl]) { //upwards port
            p = port - param_down[lvl];
            nl_first += sw_per_stage[lvl]; // we are connecting up, so the neighbouring level is above, we need to add the number of switches in this level
            res.node = (p * up_pow[lvl]) + (mod(pos, up_pow[lvl])) + ((pos / (param_down[lvl+1]*up_pow[lvl])) * up_pow[lvl+1]) + nl_first;
            res.port = (mod(pos / up_pow[lvl], param_down[lvl+1]));
        } 
        else { //downwards port
            nl_first -= sw_per_stage[lvl-1]; // we are connecting down, so the neighbouring level is below, we need to substract the number of switches in the previous level
            res.node = (port * up_pow[lvl-1]) + (mod(pos,up_pow[lvl-1])) + ((pos/up_pow[lvl])*up_pow[lvl-1]*param_down[lvl])  + nl_first;
            res.port = param_down[lvl-1] + mod(pos / up_pow[lvl-1], param_up[lvl-1]);
        }
    }
    return(res);
}

long is_server_forest(long i)
{
    return (i<servers);
}


char * get_network_token_forest()
{
    return network_token;
}

char * get_routing_token_forest()
{
    return routing_token;
}

char *get_routing_param_tokens_forest(long i){

    return routing_param_tokens[i];
}

char * get_topo_version_forest()
{
    return topo_version;
}

char * get_topo_param_tokens_forest(long i)
{
    return topo_param_tokens[i];
}

char * get_filename_params_forest()
{
    return filename_params;
}

long get_server_i_forest(long i)
{
    return i;
}

long get_switch_i_forest(long i)
{
    return servers+i;
}


long node_to_server_forest(long i)
{
    return i;
}

long node_to_switch_forest(long i)
{
    return i-servers;
}

long get_ports_forest()
{
    return ports;
}

/**
 * Get the number of paths between a source and a destination.
 * @return the number of paths.
 */
long get_n_paths_routing_forest(long src, long dst)
{
    long mca=0;

    switch(routing){
        case TREE_STATIC_ROUTING:
            return (1);
            break;
        case TREE_RND_ROUTING:
        case TREE_RR_ROUTING:
            while (src/down_pow[mca]!=dst/down_pow[mca]) {
                mca++;
            }
            return max(up_pow[mca],max_paths);
            break;
        default:
            printf("Not a tree-compatible routing %d", routing);
            exit(-1);
    }
}

/**
 * Simple oblivious UP/DOWN. Others can be implemented.
 */
long init_routing_forest_static(long src, long dst)
{
    long mca=0; // minimum common ancestor (levels)
    long i;

    while (src/down_pow[mca]!=dst/down_pow[mca]) {
        mca++;
    }

    cur_route[0]=0; //first hop is away from server, so no option to choose.
    for (i=1; i<mca; i++) { // Choose option based on source server, ensures load balancing.
        cur_route[i]=param_down[i-1]+((src/down_pow[i-1]) % param_up[i-1]);
    }

    for (i=0; i<mca; i++) {
        cur_route[mca+i]=(dst/down_pow[mca-1-i]) % param_down[mca-1-i];
    }

    cur_hop=0;
    return 0;
}

/**
 * Simple randomized UP/DOWN. Multipath-capable
 */
long init_routing_forest_random(long src, long dst)
{
    long mca=0; // minimum common ancestor (levels)
    long i;
    long p=rand();	// a random number used to select the path to use

    while (src/down_pow[mca]!=dst/down_pow[mca]) {
        mca++;
    }

    cur_route[0]=0; //first hop is away from server, so no option to choose.
    for (i=1; i<mca; i++) { // Choose option based on source server, ensures load balancing.
        cur_route[i]=param_down[i-1]+((p/down_pow[i-1]) % param_up[i-1]);
    }

    for (i=0; i<mca; i++) {
        cur_route[mca+i]=(dst/down_pow[mca-1-i]) % param_down[mca-1-i];
    }

    cur_hop=0;
    return 0;
}

/**
 * Simple UP/DOWN with round robin between all possible paths. Multipath-capable
 */
long init_routing_forest_roundrobin(long src, long dst)
{
    long mca=0; // minimum common ancestor (levels)
    long i;

    while (src/down_pow[mca]!=dst/down_pow[mca]) {
        mca++;
    }

    cur_route[0]=0; //first hop is away from server, so no option to choose.
    for (i=1; i<mca; i++) { // Choose option based on source server, ensures load balancing.
        cur_route[i]=param_down[i-1]+((path_index[src]/down_pow[i-1]) % param_up[i-1]);
    }

    for (i=0; i<mca; i++) {
        cur_route[mca+i]=(dst/down_pow[mca-1-i]) % param_down[mca-1-i];
    }
    path_index[src]=(path_index[src]+1)%servers;
    cur_hop=0;
    return 0;
}


/**
 * Routing selector. Others can be implemented.
 */
long init_routing_forest(long src, long dst)
{
    switch(routing){
        case TREE_STATIC_ROUTING:
            return init_routing_forest_static(src, dst);
            break;
        case TREE_RND_ROUTING:
            return init_routing_forest_random(src, dst);
            break;
        case TREE_RR_ROUTING:
            return init_routing_forest_roundrobin(src, dst);
            break;
        default:
            printf("Not a tree-compatible routing %d", routing);
            exit(-1);
    }
}

void finish_route_forest()
{

}

/**
 * Return current hop. Calculated in init_routing
 */
long route_forest(long current, long destination)
{
    return cur_route[cur_hop++];
}
